<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saudara extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_personal');
		$this->load->database();
	}

	function get(){
		$data=$this->M_personal->get_saudara();
		echo json_encode($data);
	}
	function save(){
		$data=$this->M_personal->save_saudara();
		echo json_encode($data);
	}
	function delete(){
		$data=$this->M_personal->delete_product();
		echo json_encode($data);
	}
}