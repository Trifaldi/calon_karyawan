<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	//var $uniqe = 'test1'; //session login

	public function index()
	{

		$this->data['title'] = 'Form Wizards';
		$this->load->view('components2/main', $this->data);
	}
	
	public function steep($no='') //step next
	{
		$uniqe = $this->session->userdata('uniqe'); //session unik login
		$tbl = get_tabel()[$no]; //select table berdasarkan step/nomornya
		if ($no == 3) {
			$where  = array('id_unix'=>$uniqe); // where uniqe
		}else{
			$where  = array('uniqe'=>$uniqe); // where uniqe
		}
		
		$data['baris'] = get_field($tbl, $where); //mengambil nilai dari $tbl
		$data['no']    = $no; //no aktif dihalaman
		$data['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
		$data['marital'] = $this->db->query('SELECT * FROM b_payroll_ptkp_setahun')->result();
		$data['kota'] = $this->db->query('SELECT * FROM kota_kota ORDER BY kota ASC;')->result();
		$this->load->view('template/step/'.$no, $data); //view step berdasarkan $no nya
	}

	public function simpan($no='') //simpan data
	{
		$uniqe = $this->session->userdata('uniqe'); //session unik login
		$post = get_post_all(); //ambil semua name form nya
		$tbl = get_tabel(); //get tabel array
		if (!empty($tbl) && !empty($post)) { //jika $tbl & $post nya ada
			$tblnya = $tbl[$no]; //select table berdasarkan urutan/nomor stepnya
			$where  = array('uniqe'=>$uniqe); // where uniqe
			if (get($tblnya, $where)->num_rows()==0) { //cek data jika belum ada maka di insert 
				$post['uniqe'] = $uniqe; //menambahkan baris untuk disimpan
				add_data($tblnya,$post); //simpan data
			}else{ // jika sudah ada maka di update
				update_data($tblnya,$post,$where); // update data
			}
		}
		
	}

}
