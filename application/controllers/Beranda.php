<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_m');
		$this->load->database();
		if(!$this->session->userdata('hp_1')){
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->helper('url');
		$uniqe = $this->session->userdata('uniqe');
		$this->data['personal'] = $this->db->query("SELECT * FROM b_personal WHERE uniqe = '$uniqe' ")->row();
		$this->data['ayah'] = $this->db->query("SELECT * FROM p_familiy WHERE id_unix = '$uniqe' AND fam_ket = 'Ayah' ")->row();
		$this->data['ibu'] = $this->db->query("SELECT * FROM p_familiy WHERE id_unix = '$uniqe' AND fam_ket = 'Ibu' ")->row();
		$this->data['saudara1'] = $this->db->query("SELECT * FROM p_familiy WHERE id_unix = '$uniqe' AND fam_ket = 'Keterangan Saudara1' ")->row();
		$this->data['saudara2'] = $this->db->query("SELECT * FROM p_familiy WHERE id_unix = '$uniqe' AND fam_ket = 'keterangan Saudara2' ")->row();
		$this->data['family'] = $this->db->query("SELECT * FROM p_familiy WHERE id_unix = '$uniqe' ")->row();
		$this->data['title'] = 'Dashboard';
		//$this->data['index'] = $this->input->post('index');
		$this->data['username'] = $this->session->userdata('fullname');
		$this->data['kota'] = $this->db->query('SELECT * FROM kota_kota ORDER BY kota ASC;')->result();
		$this->data['pendidikan'] = $this->db->query('SELECT * FROM p_education_level;')->result();
		$this->data['martial'] = $this->db->query('SELECT * FROM b_payroll_ptkp_setahun')->result();
		$this->data['subview'] = 'dashboard/main';
		$this->load->view('components/main', $this->data);
		//$this->load->view('welcome_message', $this->data);

		//log_r($this->data);
	}

	public function create()
	{		

		$uniqe = $this->session->userdata('uniqe');
		$data = array(
			'fullname' 		=> $this->input->post('fullname'), 
			'no_ktp'  		=> $this->input->post('no_ktp'),
			'place_birth'  	=> $this->input->post('place_birth'),
			'date_birth'  	=> $this->input->post('date_birth'),
			'jenis_kelamin'	=> $this->input->post('jenis_kelamin'),
			'address'  		=> $this->input->post('address'),
			'hp_1' 			=> $this->input->post('hp_1'),
			'hp_2' 			=> $this->input->post('hp_2'),
			'religion'  	=> $this->input->post('religion'),
			'marital'  		=> $this->input->post('marital'),
			'hobby' 		=> $this->input->post('hobby'),
			'kacamata'   	=> $this->input->post('kacamata')
		);
		//log_r($data);
		$this->load->model('M_personal');
		$update = $this->M_personal->update($data,$uniqe);
		header('Content-Type: application/json');
		echo json_encode($update);
	}
	public function create2()
	{	

		$nama_file = time().'.jpg';
		$direktori = 'assets/uploads/';
		$target = $direktori.$nama_file;
		move_uploaded_file($_FILES['webcam']['tmp_name'], $target);

		
		$uniqe 					= $this->session->userdata('uniqe');
		$darah  				= $this->input->post('darah'); 
		$email 					= $this->input->post('email');
		$telegram  				= $this->input->post('telegram');
		$facebook  				= $this->input->post('facebook');
		$twitter   				= $this->input->post('twitter');
		$instagram 				= $this->input->post('instagram');
		$kendaraan 				= $this->input->post('kendaraan');
		$jumlah_saudara 		= $this->input->post('jumlah_saudara');
		$foto 			= $nama_file;
		$data = array(
			'darah' 			=> $darah, 
			'email' 			=> $email,
			'telegram'  		=> $telegram,
			'facebook'  		=> $facebook,
			'twitter'  			=> $twitter,
			'instagram'			=> $instagram,
			'kendaraan'			=> $kendaraan,
			'jumlah_saudara'	=> $jumlah_saudara,
			'foto'	=> $foto
		); 

		//log_r($data);
		$this->load->model('M_personal');
		$insert = $this->M_personal->update($data,$uniqe);
		header('Content-Type: application/json');
		echo json_encode($insert);
	}
	function get_barang(){
		$uniq=$this->input->get('uniq');
		$data=$this->M_personal->get_personal_by_kode($uniq);
		echo json_encode($data);
	}

	function create3(){
		$uniqe = $this->session->userdata('uniqe');
				// Ambil data yang dikirim dari form
		$id_unix = $_POST['id_unix']; // Ambil data nis dan masukkan ke variabel nis
		$fam_ket = $_POST['fam_ket']; // Ambil data fam_ket dan masukkan ke variabel fam_ket
		$fam_nama = $_POST['fam_nama']; // Ambil data fam_nama dan masukkan ke variabel fam_nama
		$fam_sttus = $_POST['fam_sttus'];
		$fam_jekel = $_POST['fam_jekel'];
		$fam_no_hp = $_POST['fam_no_hp'];
		$fam_usia = $_POST['fam_usia'];
		$fam_pen = $_POST['fam_pen'];
		$fam_job = $_POST['fam_job'];
		$fam_per = $_POST['fam_per'];
		$fam_kategori = $_POST['fam_kategori']; // Ambil data alamat dan masukkan ke variabel alamat
		$data = array();
		
		$x = 0;
		while($x<count($fam_nama)){

			array_push($data, array(
				'id_unix'=> $id_unix[$x],
				'fam_ket'=> $fam_ket[$x],  // Ambil dan set data fam_ket sesuai index array dari $index
				'fam_nama'=> $fam_nama[$x],  // Ambil dan set data telepon sesuai index array dari $index
				'fam_sttus'=> $fam_sttus[$x],
				'fam_jekel'=> $fam_jekel[$x],
				'fam_no_hp'=> $fam_no_hp[$x],
				'fam_usia'=> $fam_usia[$x],  // Ambil dan set data alamat sesuai index array dari $index
				'fam_pen'=> $fam_pen[$x],
				'fam_job'=> $fam_job[$x],
				'fam_per'=> $fam_per[$x],
				'fam_kategori'=> $fam_kategori[$x],
			));
			$x++;
		}


		$this->load->model('M_personal');
		$this->db->query(" UPDATE b_personal SET proses_status = 1 WHERE uniqe = '$uniqe' ");
		//if ($uniqe !== $id_unix) {
		$insert = $this->M_personal->save_batch($data);
		//} else {
			//$insert = $this->M_personal->update_batch($data,$id_unix,$fam_ket);
		//}
		
		header('Content-Type: application/json');
		echo json_encode($insert);
	}
	function create3a(){
		//$uniqe = $this->session->userdata('uniqe');
				// Ambil data yang dikirim dari form
		$fam_kategori = $_POST['fam_kategori'];
		$id_unix = $_POST['id_unix']; // Ambil data nis dan masukkan ke variabel nis
		$fam_ket = $_POST['fam_ket']; // Ambil data fam_ket dan masukkan ke variabel fam_ket
		$fam_nama = $_POST['fam_nama']; // Ambil data fam_nama dan masukkan ke variabel fam_nama
		$fam_sttus = $_POST['fam_sttus'];
		$fam_jekel = $_POST['fam_jekel'];
		$fam_no_hp = $_POST['fam_no_hp'];
		$fam_usia = $_POST['fam_usia'];
		$fam_pen = $_POST['fam_pen'];
		$fam_job = $_POST['fam_job'];
		$fam_per = $_POST['fam_per']; // Ambil data alamat dan masukkan ke variabel alamat
		$data = array();

		$x = 0;
		foreach($fam_kategori as $dataid_fam){ 
			array_push($data, array(
				'fam_kategori'=> $dataid_fam,
				'id_unix'=> $id_unix[$x],
				'fam_ket'=> $fam_ket[$x],  // Ambil dan set data fam_ket sesuai index array dari $index
				'fam_nama'=> $fam_nama[$x],  // Ambil dan set data telepon sesuai index array dari $index
				'fam_sttus'=> $fam_sttus[$x],
				'fam_jekel'=> $fam_jekel[$x],
				'fam_no_hp'=> $fam_no_hp[$x],
				'fam_usia'=> $fam_usia[$x],  // Ambil dan set data alamat sesuai index array dari $index
				'fam_pen'=> $fam_pen[$x],
				'fam_job'=> $fam_job[$x],
				'fam_per'=> $fam_per[$x],
			));
			$x++;
		}
				  $this->db->where_in('id_unix',$id_unix);
		$insert = $this->db->update_batch('p_familiy', $data,'fam_kategori');
		header('Content-Type: application/json');
		echo json_encode($insert);
	}
	function create4(){
		$uniqe = $this->session->userdata('uniqe');
				// Ambil data yang dikirim dari form
		$id_unix = $_POST['id_unix']; // Ambil data nis dan masukkan ke variabel nis
		$fam_ket = $_POST['fam_ket']; // Ambil data fam_ket dan masukkan ke variabel fam_ket
		$fam_nama = $_POST['fam_nama']; // Ambil data fam_nama dan masukkan ke variabel fam_nama
		$fam_jekel = $_POST['fam_jekel'];
		$fam_no_hp = $_POST['fam_no_hp'];
		$fam_usia = $_POST['fam_usia'];
		$fam_pen = $_POST['fam_pen'];
		$fam_job = $_POST['fam_job'];
		$fam_per = $_POST['fam_per']; // Ambil data alamat dan masukkan ke variabel alamat
		$data = array();
		
		$x = 0;
		while($x<count($fam_nama)){

			array_push($data, array(
				'id_unix'=> $id_unix[$x],
				'fam_ket'=> $fam_ket[$x],  // Ambil dan set data fam_ket sesuai index array dari $index
				'fam_nama'=> $fam_nama[$x],  // Ambil dan set data telepon sesuai index array dari $index
				'fam_jekel'=> $fam_jekel[$x],
				'fam_no_hp'=> $fam_no_hp[$x],
				'fam_usia'=> $fam_usia[$x],  // Ambil dan set data alamat sesuai index array dari $index
				'fam_pen'=> $fam_pen[$x],
				'fam_job'=> $fam_job[$x],
				'fam_per'=> $fam_per[$x],
			));
			$x++;
		}


		$this->load->model('M_personal');
		$this->db->query(" UPDATE b_personal SET proses_status = 2 WHERE uniqe = '$uniqe' ");
		//if ($uniqe !== $id_unix) {
		$insert = $this->M_personal->save_batch($data);
		//} else {
			//$insert = $this->M_personal->update_batch($data,$id_unix,$fam_ket);
		//}
		
		header('Content-Type: application/json');
		echo json_encode($insert);
	}
	function create4a(){
		//$uniqe = $this->session->userdata('uniqe');
		//Ambil data yang dikirim dari form
		$id_fam = $_POST['id_fam'];
		$id_unix = $_POST['id_unix']; // Ambil data nis dan masukkan ke variabel nis
		$fam_ket = $_POST['fam_ket']; // Ambil data fam_ket dan masukkan ke variabel fam_ket
		$fam_nama = $_POST['fam_nama']; // Ambil data fam_nama dan masukkan ke variabel fam_nama
		$fam_jekel = $_POST['fam_jekel'];
		$fam_no_hp = $_POST['fam_no_hp'];
		$fam_usia = $_POST['fam_usia'];
		$fam_pen = $_POST['fam_pen'];
		$fam_job = $_POST['fam_job'];
		$fam_per = $_POST['fam_per']; // Ambil data alamat dan masukkan ke variabel alamat
		$data = array();
		
		$x = 0;
		foreach($id_fam as $dataid_fam){ 
			array_push($data, array(
				'id_fam'=> $dataid_fam,
				'id_unix'=> $id_unix[$x],
				'fam_ket'=> $fam_ket[$x],  // Ambil dan set data fam_ket sesuai index array dari $index
				'fam_nama'=> $fam_nama[$x],  // Ambil dan set data telepon sesuai index array dari $index
				'fam_jekel'=> $fam_jekel[$x],
				'fam_no_hp'=> $fam_no_hp[$x],
				'fam_usia'=> $fam_usia[$x],  // Ambil dan set data alamat sesuai index array dari $index
				'fam_pen'=> $fam_pen[$x],
				'fam_job'=> $fam_job[$x],
				'fam_per'=> $fam_per[$x],
			));
			$x++;
		}
		$insert = $this->db->update_batch('p_familiy', $data,'id_fam');
		header('Content-Type: application/json');
		echo json_encode($insert);
	}

	function create5(){
		$uniqe = $this->session->userdata('uniqe');
				// Ambil data yang dikirim dari form
		$id_unix = $_POST['id_unix']; // Ambil data nis dan masukkan ke variabel nis
		$fam_ket = $_POST['fam_ket']; // Ambil data fam_ket dan masukkan ke variabel fam_ket
		$fam_nama = $_POST['fam_nama']; // Ambil data fam_nama dan masukkan ke variabel fam_nama
		$fam_jekel = $_POST['fam_jekel'];
		$fam_no_hp = $_POST['fam_no_hp'];
		$fam_usia = $_POST['fam_usia'];
		$fam_pen = $_POST['fam_pen'];
		$fam_job = $_POST['fam_job'];
		$fam_per = $_POST['fam_per']; // Ambil data alamat dan masukkan ke variabel alamat
		$data = array();
		
		$x = 0;
		while($x<count($fam_nama)){

			array_push($data, array(
				'id_unix'=> $id_unix[$x],
				'fam_ket'=> $fam_ket[$x],  // Ambil dan set data fam_ket sesuai index array dari $index
				'fam_nama'=> $fam_nama[$x],  // Ambil dan set data telepon sesuai index array dari $index
				'fam_jekel'=> $fam_jekel[$x],
				'fam_no_hp'=> $fam_no_hp[$x],
				'fam_usia'=> $fam_usia[$x],  // Ambil dan set data alamat sesuai index array dari $index
				'fam_pen'=> $fam_pen[$x],
				'fam_job'=> $fam_job[$x],
				'fam_per'=> $fam_per[$x],
			));
			$x++;
		}

		$this->load->model('M_personal');
		$this->db->query(" UPDATE b_personal SET proses_status = 3 WHERE uniqe = '$uniqe' ");
		$insert = $this->M_personal->save_batch($data);
		header('Content-Type: application/json');
		echo json_encode($insert);

	}
	function create5a(){
		//$uniqe = $this->session->userdata('uniqe');
		//Ambil data yang dikirim dari form
		$id_fam = $_POST['id_fam'];
		$id_unix = $_POST['id_unix']; // Ambil data nis dan masukkan ke variabel nis
		$fam_ket = $_POST['fam_ket']; // Ambil data fam_ket dan masukkan ke variabel fam_ket
		$fam_nama = $_POST['fam_nama']; // Ambil data fam_nama dan masukkan ke variabel fam_nama
		$fam_jekel = $_POST['fam_jekel'];
		$fam_no_hp = $_POST['fam_no_hp'];
		$fam_usia = $_POST['fam_usia'];
		$fam_pen = $_POST['fam_pen'];
		$fam_job = $_POST['fam_job'];
		$fam_per = $_POST['fam_per']; // Ambil data alamat dan masukkan ke variabel alamat
		$data = array();
		
		$x = 0;
		foreach($id_fam as $dataid_fam){ 
			array_push($data, array(
				'id_fam'=> $dataid_fam,
				'id_unix'=> $id_unix[$x],
				'fam_ket'=> $fam_ket[$x],  // Ambil dan set data fam_ket sesuai index array dari $index
				'fam_nama'=> $fam_nama[$x],  // Ambil dan set data telepon sesuai index array dari $index
				'fam_jekel'=> $fam_jekel[$x],
				'fam_no_hp'=> $fam_no_hp[$x],
				'fam_usia'=> $fam_usia[$x],  // Ambil dan set data alamat sesuai index array dari $index
				'fam_pen'=> $fam_pen[$x],
				'fam_job'=> $fam_job[$x],
				'fam_per'=> $fam_per[$x],
			));
			$x++;
		}
		$insert = $this->db->update_batch('p_familiy', $data,'id_fam');
		header('Content-Type: application/json');
		echo json_encode($insert);
	}
}
