<?php $this->load->view('components2/head'); ?>
<style type="text/css">
	#myModal {
		width: 100%;
		height: 100%;
		position: fixed;
		background: rgba(0,0,0,.7);
		top: 0;
		left: 0;
		z-index: 9999;
	}
</style>
<style>
  #bg-watermark{
    position:fixed; z-index:99999; background:rgba(0,0,0,0.8); display:block;
    padding:10px; padding-top:4px; padding-bottom:4px; margin-bottom: 43%; right:0; top:0; cursor: pointer;
  	margin-top:10px; padding:10px;
  }
  #text-watermark{
    color:black; font-size:20px; cursor: pointer;
  }
</style>
<style type="text/css">
	.select2-container--default .select2-selection--single {
		background-color: #fff;
		border: 1px solid #0000ff;
		border-top: none;
		border-left: none;
		border-right: none;
		border-radius: 0px;
	}
	.wrapper .main .content .content-box .form-wizard-nav .step:before, .wrapper .main .content .media-wrapper .media-row .media-box .form-wizard-nav .step:before, .wrapper .main .content .invoice-wrapper .form-wizard-nav .step:before{
		background-color: #0275d8;
	}
	.form-control{border:none;border-bottom: 1px solid blue;border-radius:0px;}
	label{color:#1e88e5;margin-bottom:-10px;font-size: 15px}
	.form-group{margin-bottom:30px; margin-top: 15px;}
	::-webkit-input-placeholder { /* Edge */
		color: #a2a5a7;
	}

	:-ms-input-placeholder { /* Internet Explorer 10-11 */
		color: #a2a5a7;
	}

	::placeholder {
		color: #a2a5a7 !important;
	}
	.responsive-height{ min-height: 480px; margin-bottom: -10px;}
	@media screen and (max-width: 700px)
	{
		.responsive-height{ min-height: 400px; }
	}
	@media screen and (max-width: 800px)
	{
		.responsive-height{ min-height: 500px; }
	}

	.jarak_kiri{ padding-left:40px; }
	.jarak_kanan{ padding-right:40px; }
	@media screen and (max-width: 575px)
	{
		.jarak_kiri{ padding-left:0px;padding-right:0px; }
		.jarak_kanan{ padding-left:0px;padding-right:0px; }
	}

</style>
<script src="<?= base_url();?>template/datepicker.min.js"></script>
<script src="<?= base_url();?>template/i18n/datepicker.en.js"></script>
<div id="myModal" class="modal fade animated bounceInDown" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content" style="width: 1000px;right: 106px;top: 47px;border-top-width: 0px;margin-top: 0px;">
      <div class="modal-header" style="padding-top: 27px;border-bottom-width: 0px;">
        <button type="button" class="close" data-dismiss="modal"></button>
        <h4 class="modal-title" style="font-size: 29px;font-style: normal;color: red;"><label class="label label-warning" style="margin-right: 62px; font-size: 31px;">HAI, <?php echo $this->session->userdata('fullname') ?></label></h4>
      </div>
      <div class="modal-body" style="text-align: center;font-size: 29px;margin-left: 21px;margin-right: 8px;">
        <h4 style="text-align: center;">Anda akan melakukan registrasi kekaryawanan villacorp.</h4>
        <br>
        <p>Jika terjadi logout, login kembali dengan nomor handphone <label style = "color: red; font-size: 22px;"><?php echo $this->session->userdata('hp_1') ?></label> yang sudah anda daftarkan.</p>
        <p>Dengan cara klik tombol home <a><i class="fa fa-home" style="color: green;font-size: 30px;"></i></a> pada form register.</p>
      </div>
      <div class="modal-footer" style=" padding-bottom: 20px; padding-top: 85px;">
        <button type="button" class="btn btn-success" data-dismiss="modal">NEXT</button>
      </div>
    </div>

  </div>
</div>
<div id="bg-watermark" onclick="bg_wm()" class="animated bounceInLeft" style="border-top-left-radius: 20px;border-bottom-left-radius: 20px;background: #5bc0de;">
      <p id="text-watermark" style="font-size: 20px;">
        <i class="fa fa-user" style="font-size: 20px;"></i>&nbsp;<span id="h-wm"><br></span> 
</div>
<div class="wrapper" style="margin-top: 0px;" onclick="bg_wn()">
	<div class="main" id="v_wizard">
		<?php //$this->load->view($subview); ?>		
	</div>
</div>
<script type="text/javascript">
step();
function step(no='',aksi='') {
	// if (no == "") { no = 1; }
	if (no=='') {
		step_next(1);
	}else{
		if (aksi=='' || aksi=='finish') {
			simpan(no, aksi);	
		}else{
			step_next(no);
		}
	}
}

function simpan(no, aksi='')
{
	wajib_isi=0;
	no_id = no-1;
	id_form = '#form-step-'+no_id;
	var form = $(id_form+" input, "+id_form+" select");
	var datanya = new FormData();
	form.each(function(){
		if ($(this).prop("required")) {
			if ($(this).val()=='') {
				// console.log('wajib isi');
				wajib_isi = 1;
			}
		}else{
			wajib_isi = 0;
		}
		datanya.append($(this).attr("name"), $(this).val());
	});

	if (wajib_isi==0) {
		$('#v_wizard').empty();
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url('welcome/simpan')?>/"+no_id,
			data: datanya,
	        processData: false,
	        contentType: false,
			chace: false,
			beforeSend: function() {
				$('#v_wizard').html('Loading . . .');
			},
			success: function(data) {
				//console.log(data);
				if (aksi=='finish') { // finish
					$('#v_wizard').html('Finish . . .');
				}else{
					step_next(no);
				}
			}
		});
	}
 
}

function step_next(no)
{
	$.ajax({
		type: 'POST',
		url: "<?php echo base_url('welcome/steep')?>/"+no,
		// async: false,
		chace: false,
		beforeSend: function() {
			$('#v_wizard').html('Loading . . .');
		},
		success: function(data) {
			//console.log(data);
			$('#v_wizard').html(data);
		}
	});
}
</script>
<script type="text/javascript">
	$('#myModal').modal('show');
</script>
<script type="text/javascript">
      function setCookie(key, value, expiry) {
          var expires = new Date();
          expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
          document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
      }

      function getCookie(key) {
          var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
          return keyValue ? keyValue[3] : null;
      }

      function eraseCookie(key) {
          var keyValue = getCookie(key);
          setCookie(key, keyValue, '-1');
      }
      bg_wm(1);
      function bg_wm(aksi='')
      {
        bg = $('#bg-watermark');
        if (aksi=='1') {
          if (getCookie('watermark')==1) {
            $('#h-wm').html('<?php echo $this->session->userdata('nama') ?><br/><i class="fa fa-phone">&nbsp;&nbsp;</i><?php echo $this->session->userdata('no_hp')?></p>');
            $('#text-watermark').css('font-size',20);
          }else {
            $('#h-wm').html('&nbsp;');
            $('#text-watermark').css('font-size',14);
          }
        }else {
          if (getCookie('watermark')==0) {
            setCookie('watermark','1','1');
            bg.removeClass('min-wm');
            $('#h-wm').html('<?php echo $this->session->userdata('fullname') ?><br/><i class="fa fa-phone">&nbsp;&nbsp;</i><?php echo $this->session->userdata('hp_1')?></p>');
            $('#text-watermark').css('font-size',20);
          }else {
            setCookie('watermark','0','1');
            bg.addClass('min-wm');
            $('#h-wm').html('&nbsp;');
            $('#text-watermark').css('font-size',14);
          }
        }
      }
      bg_wn(0);
      function bg_wn(aksi='')
      {
        setCookie('watermark','0','1');
        bg.addClass('min-wm');
        $('#h-wm').html('&nbsp;');
        $('#text-watermark').css('font-size',14);
      }
    </script>
<?php $this->load->view('components2/foot'); ?>

