<style type="text/css">

  .control-label {
    color: #428bca;
  }
  input.form-control{
    border-left: none;
    border-right: none;
    border-top: none;
    border-bottom-color: blue;
    border-radius: 0px;
  }
  .select2-container .select2-choice {
    padding-left: 3px;
    margin: 0px;
    bottom: 7px;
    border: none;
  }
  select.form-control{
    border-left: none;
    border-top: none;
    border-right: none;
    border-bottom-color: blue;
    border-radius: 0px;
    }
    div#s2id_select.select2-container.form-control{
      border-left: none;
      border-top: none;
      border-right: none;
      border-bottom-color: blue;
      border-radius: inherit;
    }
    .form-group {
      padding: 5px
    }
</style>
<link href="<?= base_url();?>template/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="<?= base_url();?>template/datepicker.min.js"></script>
<script src="<?= base_url();?>template/i18n/datepicker.en.js"></script>
<form role="form" id="form-user" >
  <div class="row setup-content" id="step-1">
    <div class="col-xs-12" style="left: 0px;padding-left: 0px;padding-right: 0px;">
      <div class="col-md-6">
        <div class="form-group">
          <label class="control-label">Nama Lengkap</label>
          <input name="fullname" type="text" required="required" value="<?php echo $this->session->userdata('fullname') ?>" class="form-control"  placeholder="Enter First Name"  />
        </div>
        <div class="form-group">
          <label class="control-label">No_KTP</label>
          <input  type="text" pattern=".{0}|.{19,19}" maxlength="19" name="no_ktp" id="no_ktp" onkeypress="return angka(event)" value="<?= $personal->no_ktp ?>"  required="required" title="Either 0 OR (8 chars minimum)" class="form-control" placeholder="Enter Last Name" />
        </div>
        <div class="form-group">
          <label class="control-label">Tempat Lahir</label>
          <select id="select" class="form-control" name="place_birth" required>
            <?php foreach($kota as $cat){
              $id         = $cat->id;
              $distrik         = $cat->distrik;
              $kota         = $cat->kota;?>
              <option <?php if($id == $personal->place_birth){ echo 'selected="selected"'; } ?> value="<?php echo $id; ?>"><?php echo $kota;?>&nbsp; - &nbsp;<?php echo $distrik;?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label class="control-label">Tanggal Lahir</label>
          <input type="text" max="2014-12-31" required="required" class="form-control datepicker-here" placeholder="tgl/bln/tahun" data-language='en' id="datepicker" />
        </div>
        <div class="form-group">
          <label class="control-label">Jenis Kelamin</label>
          <div>
            <div class="col-sm-6" style="top: 10px">
              <div class="form-group">
                <input <?php $jenis_kelamin = 'PRIA'; if($jenis_kelamin==$personal->jenis_kelamin){ echo 'checked=""'; } ?> name="jenis_kelamin" type="radio" value="PRIA" required> PRIA
              </div>
            </div>
            <div class="col-sm-6" style="top: 10px">
              <div class="form-group">
                <input <?php $jenis_kelamin = 'WANITA'; if($jenis_kelamin==$personal->jenis_kelamin){ echo 'checked=""'; } ?> name="jenis_kelamin" type="radio" value=" WANITA" required> WANITA
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label">Alamat</label>
          <input name="address" maxlength="100" type="text" required="required" class="form-control" value="<?= $personal->address?>" placeholder="Enter Last Name" />
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label class="control-label">No Handpone</label>
          <input name="hp_1" value="<?php echo $this->session->userdata('hp_1');?>" type="text" required="required" class="form-control" readonly>
        </div>
        <div class="form-group">
          <label class="control-label">No Handpone II</label>
          <input id="no_hp" maxlength="16" type="text" name="hp_2"  class="form-control" placeholder="Enter Last Name" />
        </div>
        <div class="form-group">
          <label class="control-label">Agama</label>
          <select placeholder="Pilih" class="form-control" name="religion" required>
            <option></option>
            <option <?php $agama = 'ISLAM'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="ISLAM">ISLAM</option>
            <option <?php $agama = 'KRISTEN'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="KRISTEN">KRISTEN</option>
            <option <?php $agama = 'KATOLIK'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="KATOLIK">KATOLIK</option>
            <option <?php $agama = 'BUDHA'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="BUDHA">BUDHA</option>
            <option <?php $agama = 'KHONG HUCU'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="KHONG HUCU">KHONG HUCU</option>
          </select>
        </div>
        <div class="form-group">
          <label class="control-label">Status Pernikahan</label>
          <select class="form-control" name="marital" required>
          <option selected disabled></option>
          <?php foreach($martial as $cat){
            $id         = $cat->id;
            $title      = $cat->keterangan;?>
            <option <?php if($id == $personal->marital){ echo 'selected="selected"'; } ?> value="<?php echo $id; ?>"><?php echo $title; ?></option>
          <?php } ?>
        </select>
        </div>
        <div class="form-group">
          <label class="control-label">Anda Berkacamata</label>
          <div>
            <div class="col-sm-6" style="top: 10px">
              <div class="form-group">
                <input <?php $kacamata = 'YA'; if($kacamata==$personal->kacamata){ echo 'checked=""'; } ?> name="kacamata" type="radio" value="YA" required> YA
              </div>
            </div>
            <div class="col-sm-6" style="top: 10px">
              <div class="form-group">
                <input <?php $kacamata = 'TIDAK'; if($kacamata==$personal->kacamata){ echo 'checked=""'; } ?> name="kacamata" type="radio" value=" TIDAK" required> TIDAK
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label">Hobby</label>
          <input name="hobby" maxlength="100" type="text" required="required" class="form-control" value="<?= $personal->hobby;?>" placeholder="Enter Last Name" />
        </div>
      </div>
    </div>
    <button class="btn btn-primary tombol-simpan nextBtn pull-right" type="button" >Next</button>
  </div>
</form>
<script>
  function harusHuruf(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
      return false;
    return true;
  }

  function angka(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode <33 || charCode > 57) {
      return false;
    }
    return true;
  }
</script>
<script type="text/javascript">
  $('#no_ktp').click(function(event) {
    var val = $('#no_ktp').val();
    if(val==''){
      $('#no_ktp').val();  
    }
  });
  $('#no_ktp').focus(function(event) {
    $('#no_ktp').val();
  });
  $("#no_ktp").focusout(function(){
    $('#no_ktp').val();
  });

  $("#no_ktp").keyup(function(){
    var val = $('#no_ktp').val();
      //var valx = '085834246342';
      var valx = val.replace(/-/gi, "");
      console.log(valx);
      arr = valx.match(/.{1,4}/g);

      //bersihkan array jika ada yg undefined
      var i = 0;
      var string = "";
      if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
        string = arr[0];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
        string = arr[0]+"-"+arr[1];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
        string = arr[0]+"-"+arr[1]+"-"+arr[2];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
        string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
      }
      //string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
      console.log(string);
      $('#no_ktp').val('');
      $('#no_ktp').val(string);
    });
  </script>
  <script type="text/javascript">
    $('#no_hp').click(function(event) {
      var val = $('#no_hp').val();
      if(val==''){
        $('#no_hp').val('08');  
      }

    });

    $('#no_hp').focus(function(event) {
      $('#no_hp').val();
    });

    $("#no_hp").focusout(function(){
      $('#no_hp').val();
    });

    $("#no_hp").keyup(function(){
      var val = $('#no_hp').val();
      //var valx = '085834246342';
      var valx = val.replace(/-/gi, "");
      console.log(valx);
      arr = valx.match(/.{1,4}/g);

      //bersihkan array jika ada yg undefined
      var i = 0;
      var string = "";
      if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
        string = arr[0];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
        string = arr[0]+"-"+arr[1];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
        string = arr[0]+"-"+arr[1]+"-"+arr[2];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
        string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
      }

      
      //string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
      console.log(string);
      $('#no_hp').val('');
      $('#no_hp').val(string);

      if(val=='08'){
        $('#no_hp').val('08'); 
      }else if(val=='0'){
        $('#no_hp').val('08'); 
      }else if(val==''){
        $('#no_hp').val('08'); 
      }
    });
  </script>
  <script>
$(document).ready(function(){

      $('#select').select2();
      placeholder: 'Select a month';

});
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
    $(".tombol-simpan").click(function(){
      var data = $('#form-user').serialize();
      $.ajax({
        type: 'POST',
        url: "<?php echo base_url('beranda/create')?>",
        data: data,
        async: false,
        dataType: 'json',
        success: function(response) {
          console.log(data);
        }
      });
    });
  });
  </script>
  