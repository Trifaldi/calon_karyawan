<!DOCTYPE html>
<html class="app">
    <head>
        <title><?php echo $title; ?></title>
        <meta charset="utf-8">
		<meta content="ie=edge" http-equiv="x-ua-compatible">
		<meta content="template language" name="keywords">
		<meta content="John Doe" name="author">
		<meta content="Admin Template" name="description">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<link href="<?php echo base_url() . 'favicon.png'; ?>" rel="shortcut icon">
		<link href="<?php echo base_url() . 'apple-touch-icon.png'; ?>" rel="apple-touch-icon">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/bootstrap/dist/css/bootstrap.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/font-awesome/css/font-awesome.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/batch-icons/style.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/dashicons/style.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/dripicons/webfont.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/eightyshades/style.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/foundation-icons/foundation-icons.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/metrize-icons/style.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/simple-line-icons/css/simple-line-icons.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/themify-icons/themify-icons.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/type-icons/style.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/weather-icons/css/weather-icons.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/animate/animate.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/fullcalendar/dist/fullcalendar.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/datatable/media/css/jquery.dataTables.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/datatable/media/css/dataTables.bootstrap4.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/dropzone/dist/dropzone.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/select2/dist/css/select2.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/bootstrap-daterangepicker/daterangepicker.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/fancybox/dist/jquery.fancybox.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/sweetalert/dist/sweetalert.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/exort/uploader.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/jquery-treegrid/jquery.treegrid.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/css/main.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo base_url() . 'template/css/custom.css'; ?>"/>

        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/jquery/jquery-2.1.1.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/jquery-count-to/jquery.countTo.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/moment/min/moment.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/fullcalendar/dist/fullcalendar.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/chart.js/dist/Chart.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/ckeditor/ckeditor.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/datatable/media/js/jquery.dataTables.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/datatable/media/js/dataTables.bootstrap4.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/dropzone/dist/dropzone.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/select2/dist/js/select2.full.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/bootstrap-daterangepicker/daterangepicker.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/bootstrap-validator/dist/validator.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/fancybox/dist/jquery.fancybox.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/disabler-enabler/disabler.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/disabler-enabler/enabler.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/sweetalert/dist/sweetalert.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/super-datagrid/datagrid.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/exort/uploader.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/jquery-treegrid/jquery.treegrid.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/slimscroll/jquery.slimscroll.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/tether/dist/js/tether.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/bootstrap/dist/js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'template/js/app.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/datetimepicker/bootstrap-datetimepicker.min.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/datetimepicker/bootstrap-datetimepicker.min.css'; ?>"/>
    </head>
    <body>
