<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="<?php echo base_url() . 'favicon.png'; ?>" rel="shortcut icon">
  <link href="<?php echo base_url() . 'apple-touch-icon.png'; ?>" rel="apple-touch-icon">
  <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/font-awesome/css/font-awesome.min.css'; ?>"/>
  <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/batch-icons/style.css'; ?>"/>
  <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/dashicons/style.css'; ?>"/>
  <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/dripicons/webfont.css'; ?>"/>
  <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/foundation-icons/foundation-icons.css'; ?>"/>
  <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/metrize-icons/style.css'; ?>"/>
  <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/simple-line-icons/css/simple-line-icons.css'; ?>"/>
  <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/themify-icons/themify-icons.css'; ?>"/>
  <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/type-icons/style.css'; ?>"/>
  <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/weather-icons/css/weather-icons.min.css'; ?>"/>
  <link rel="stylesheet" href="<?php echo base_url() . 'template/plugins/animate/animate.css'; ?>"/>
  <link href="<?php echo $this->config->base_url(); ?>assets/plugins/select2/select2.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/datetimepicker/bootstrap-datetimepicker.min.css'; ?>"/>
  <style type="text/css">

    .body{ 
      margin-top:40px; 
    }

    .stepwizard-step p {
      margin-top: 10px;
    }

    .stepwizard-row {
      display: table-row;
    }

    .stepwizard {
      display: table;
      width: 100%;
      position: relative;
    }

    .stepwizard-step button[disabled] {
      opacity: 1 !important;
      filter: alpha(opacity=100) !important;
    }

    .stepwizard-row:before {
      top: 14px;
      bottom: 0;
      position: absolute;
      content: " ";
      width: 100%;
      height: 1px;
      background-color: #ccc;
      z-order: 0;

    }

    .stepwizard-step {
      display: table-cell;
      text-align: center;
      position: relative;
    }

    .btn-circle {
      width: 30px;
      height: 30px;
      text-align: center;
      padding: 6px 0;
      font-size: 12px;
      line-height: 1.428571429;
      border-radius: 15px;
    }

  </style>
  <link href="<?php echo base_url(); ?>template/baru/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="<?php echo base_url(); ?>template/baru/bootstrap.min.js" ></script>
  <script src="<?php echo base_url(); ?>template/baru/jquery-1.11.1.min.js"></script>
  <script src="<?php echo base_url(); ?>template/baru/jquery-1.11.1.js"></script>
  <script src="<?= base_url()?>assets/js/jquery-1.11.0.js"></script>
  <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() . 'template/plugins/dropzone/dist/dropzone.js'; ?>"></script>
  <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/plugins/select2/select2.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() . 'assets/datetimepicker/bootstrap-datetimepicker.min.js'; ?>"></script>
  <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/moment.js"></script>
</head>
<body style="margin-top: 50px;">
  <style>
    #bg-watermark{
      position:fixed; z-index:99999; background:rgba(0,0,0,0.8); display:block;
      padding:10px; padding-top:4px; padding-bottom:4px; margin-bottom: 43%; right:0; top:0; cursor: pointer;
      margin-top:10px; padding:10px;
    }
    #text-watermark{
      color:black; font-size:20px; cursor: pointer;
    }
  </style>
  <div id="bg-watermark" onclick="bg_wm()" class="animated bounceInLeft" style="border-top-left-radius: 20px;border-bottom-left-radius: 20px;background: #5bc0de;">
    <p id="text-watermark" style="font-size: 20px;">
      <i class="fa fa-user" style="font-size: 20px;"></i>&nbsp;<span id="h-wm"><br></span> 
    </div>
    <div class="wrapper" style="margin-top: 0px;" onclick="bg_wn()">
      <div class="container">
        <div class="stepwizard" style="bottom: 30px">
          <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
              <a href="#step-1" type="button" class="btn btn-primary btn-circle"></i></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled"></i></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
            <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"></a>
            </div>
          </div>
        </div>
        <!-- Form 1 -->
        <?php $max_step=2; ?>
        <?php for ($i=1; $i <=$max_step; $i++) { ?>
          <?php $this->load->view('form/'.$i.''); ?>
        <?php } ?>
        

        <!-- Form 2 -->
        <form role="form">
          <div class="row setup-content" id="step-2">
            <div class="col-xs-12">
              <div class="col-md-12">
                <h3> Step 2</h3>
                <div class="form-group">
                  <label class="control-label">No Handpone</label>
                  <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Name" />
                </div>
                <div class="form-group">
                  <label class="control-label">No Handpone II</label>
                  <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Address"  />
                </div>
                <div class="form-group">
                  <label class="control-label">No Handpone II</label>
                  <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Address"  />
                </div>
                <button type="button" class="btn btn-warning prevBtn">Previous</button>
                <button class="btn btn-primary nextBtn  pull-right" type="button" >Next</button>
              </div>
            </div>
          </div>  
        </form>
        <div class="row setup-content" id="step-3">
          <div class="col-xs-12">
            <div class="col-md-12">
              <h3> Step 3</h3>
              <button class="btn btn-success btn-lg pull-right" type="submit">Finish!</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $(document).ready(function () {
        var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
          e.preventDefault();
          var $target = $($(this).attr('href')),
          $item = $(this);

          if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
          }
        });

        allNextBtn.click(function(){
          var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

          $(".form-group").removeClass("has-error");
          for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
          }

          if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
      });
    </script>
    <style type="text/css">
      #myModal {
        width: 100%;
        height: 100%;
        position: fixed;
        background: rgba(0,0,0,.7);
        top: 0;
        left: 0;
        z-index: 9999;
      }
    </style>
    <div id="myModal" class="modal fade animated bounceInDown" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content" style="width: 1000px;right: 61px;top: 47px;border-top-width: 0px;margin-top: 0px;">
          <div class="modal-header" style="padding-top: 27px;border-bottom-width: 0px;">
            <button type="button" class="close" data-dismiss="modal"></button>
            <h4 class="modal-title" style="font-size: 29px;font-style: normal;color: red;"><label class="label label-warning" style="margin-right: 62px; font-size: 31px;">HAI, <?php echo $this->session->userdata('fullname') ?></label></h4>
          </div>
          <div class="modal-body" style="text-align: center;font-size: 29px;margin-left: 21px;margin-right: 8px;">
            <h4 style="text-align: center;">Anda akan melakukan registrasi kekaryawanan villacorp.</h4>
            <br>
            <p>Jika terjadi logout, login kembali dengan nomor handphone <label style = "color: red; font-size: 22px;"><?php echo $this->session->userdata('hp_1') ?></label> yang sudah anda daftarkan.</p>
            <p>Dengan cara klik tombol home <a><i class="fa fa-home" style="color: green;font-size: 30px;"></i></a> pada form register.</p>
          </div>
          <div class="modal-footer" style=" padding-bottom: 20px; padding-top: 85px;">
            <button type="button" class="btn btn-success" data-dismiss="modal">NEXT</button>
          </div>
        </div>

      </div>
      <script>
        $(document).ready(function(){
          $("#myModal").modal('show');
        });
      </script>
      <script type="text/javascript">
        function setCookie(key, value, expiry) {
          var expires = new Date();
          expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
          document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        }

        function getCookie(key) {
          var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
          return keyValue ? keyValue[2] : null;
        }

        function eraseCookie(key) {
          var keyValue = getCookie(key);
          setCookie(key, keyValue, '-1');
        }
        bg_wm(1);
        function bg_wm(aksi='')
        {
          bg = $('#bg-watermark');
          if (aksi=='1') {
            if (getCookie('watermark')==1) {
              $('#h-wm').html('<?php echo $this->session->userdata('nama') ?><br/><i class="fa fa-phone">&nbsp;&nbsp;</i><?php echo $this->session->userdata('no_hp')?></p>');
              $('#text-watermark').css('font-size',20);
            }else {
              $('#h-wm').html('&nbsp;');
              $('#text-watermark').css('font-size',14);
            }
          }else {
            if (getCookie('watermark')==0) {
              setCookie('watermark','1','1');
              bg.removeClass('min-wm');
              $('#h-wm').html('<?php echo $this->session->userdata('fullname') ?><br/><i class="fa fa-phone">&nbsp;&nbsp;</i><?php echo $this->session->userdata('hp_1')?></p>');
              $('#text-watermark').css('font-size',20);
            }else {
              setCookie('watermark','0','1');
              bg.addClass('min-wm');
              $('#h-wm').html('&nbsp;');
              $('#text-watermark').css('font-size',14);
            }
          }
        }
        bg_wn(0);
        function bg_wn(aksi='')
        {
          setCookie('watermark','0','1');
          bg.addClass('min-wm');
          $('#h-wm').html('&nbsp;');
          $('#text-watermark').css('font-size',14);
        }
      </script>
    </body>
    </html>