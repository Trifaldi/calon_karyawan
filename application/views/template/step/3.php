<style type="text/css">
	.select2-container--default .select2-selection--single {
		background-color: #fff;
		border: 1px solid #0000ff;
		border-top: none;
		border-left: none;
		border-right: none;
		border-radius: 0px;
	}
	.wrapper .main .content .content-box .form-wizard-nav .step:before, .wrapper .main .content .media-wrapper .media-row .media-box .form-wizard-nav .step:before, .wrapper .main .content .invoice-wrapper .form-wizard-nav .step:before{
		background-color: #0275d8;
	}
	.form-control{border:none;border-bottom: 1px solid blue;border-radius:0px;}
	label{color:#1e88e5;margin-bottom:-10px;font-size: 15px}
	.form-group{margin-bottom:30px; margin-top: 15px;}
	::-webkit-input-placeholder { /* Edge */
		color: #a2a5a7;
	}
	:-ms-input-placeholder { /* Internet Explorer 10-11 */
		color: #a2a5a7;
	}
	::placeholder {
		color: #a2a5a7 !important;
	}
	.responsive-height{ min-height: 480px; margin-bottom: -10px;}
	@media screen and (max-width: 700px)
	{
		.responsive-height{ min-height: 400px; }
	}
	@media screen and (max-width: 800px)
	{
		.responsive-height{ min-height: 500px; }
	}
	.jarak_kiri{ padding-left:40px; }
	.jarak_kanan{ padding-right:40px; }
	@media screen and (max-width: 575px)
	{
		.jarak_kiri{ padding-left:0px;padding-right:0px; }
		.jarak_kanan{ padding-left:0px;padding-right:0px; }
	}

</style>
<script src="https://bossanova.uk/jsuites/v2/jsuites.js"></script>
<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css" />
<script src="<?= base_url();?>template/datepicker.min.js"></script>
<script src="<?= base_url();?>template/i18n/datepicker.en.js"></script>
<div class="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;padding-bottom: 0px;">
	<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
		<div class="col-md-12">
			<div class="form-wizard-nav">
				<?php for ($i=1; $i <=max_step(); $i++) { ?>
				<?php if ($i<=$no){ ?>
				<div class="step complete active" data-form="#form-<?php echo $i; ?>"></div>
				<?php }else{ ?>
				<div class="step" data-form="#form-<?php echo $i; ?>"></div>
				<?php } ?>
				<?php } ?>
			</div>
		</div>
		<div class="col-md-12" id="form-1">
			<form id="form-step-1">
				<div class="row">
					<div class="col-sm-6 jarak_kanan">
						<div class="col-md-12">
							<div class="form-group">
								<label >Nama Ayah</label>
								<input type="text" class="form-control" name="fam_nama" value="<?php echo $baris['fam_nama'];?>">
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Status</label>
								<select name="fam_sttus[]" class="form-control" id="mySelect" onchange="myFunction(event)" >
							      <option>Pilih..</option>
							      <option value="Masih Hidup">Masih Hidup</option>
							      <option value="Almarhum">Almarhum</option>
							    </select>
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<p id="demo"></p>
						<p id="demo2"></p>
						
					</div>
					<div class="col-sm-6 jarak_kiri">
						<div class="col-md-12">
							<div class="form-group">
								<label>Nama Ibu</label>
								<input type="text" class="form-control" name="fam_nama" value="<?php echo $baris['fam_nama'];?>">
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Status</label>
								<select class="form-control" id="mySelect3" name="<?php echo $baris['fam_nama'];?>" onchange="myFunction3(event)">
									<option>Pilih..</option>
									<option value="Masih Hidup">Masih Hidup</option>
      								<option value="Almarhum">Almarhum</option>
								</select>
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<p id="demo3"></p>
						<p id="demo4"></p>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
	<div class="content">
			<div class="content-box-footer" style="position: relative;">
				<a onclick="step(2,'prev')" class="btn btn-warning prev-action">Back</a>
				<a onclick="step(4)" class="btn btn-success next-action pull-right ">Next</a>
			</div>
	</div>
<script>
	function harusHuruf(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
			return false;
		return true;
	}
	function angka(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode <33 || charCode > 57) {
			return false;
		}
		return true;
	}
</script>
<script type="text/javascript">
  function myFunction(e) {
    var x = document.getElementById("mySelect").value;
    if (x == 'Masih Hidup') {
      document.getElementById("demo").innerHTML='<div class="col-md-12">'+
      '<div class="form-group">'+
      '<label >No Hape Ayah</label>'+
      '<input class="form-control" maxlength="16"onclick="myNumber()" id="hp_ayah" type="text" onkeypress="return angka(event)" name="fam_no_hp[]" placeholder="No Hp Ayah" data-mask="0000-0000-0000-0">'+
      '<div class="help-block form-text text-muted form-control-feedback"></div>'+
      '</div>'+
      '</div>'+
      '<div class="col-md-12">'+
      '<div class="form-group">'+
      '<label >Usia</label>'+
      '<input class="form-control" type="text" name="fam_usia[]" maxlength="3" onkeypress="return angka(event)">'+
      '<div class="help-block form-text text-muted form-control-feedback">'+
      '</div>'+
      '</div>'+
      '</div>'+
      '<div class="col-md-12">'+
      '<div class="form-group">'+
      '<label >Pendidikan</label>'+
      '<select name="fam_pen[]" class="form-control">'+
      '<option>Pilih..</option>'+
      '<?php foreach ($pendidikan as $key => $value) { ?>'+
      '<option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>'+
      '<?php } ?>'+
      '</select>'+
      '<div class="help-block form-text text-muted form-control-feedback">'+
      '</div>'+
      '</div>'+
      '</div>'+
      '<div class="col-md-12">'+
      '<div class="form-group">'+
      '<label >Pekerjaan</label>'+
      '<select name="fam_job[]" class="form-control" id="mySelect2" onchange="myFunction2(event)">'+
      '<option>Pilih..</option>'+
      '<option value="Bekerja">Bekerja</option>'+
      '<option value="Tidak Bekerja">Tidak Bekerja</option>'+
      '</select>'+
      '<div class="help-block form-text text-muted form-control-feedback">'+
      '</div>'+
      '</div>'+
      '</div>';
    } else {
      document.getElementById("demo").innerHTML='';
    }
  }
  function myFunction2(e) {
    var x = document.getElementById("mySelect2").value;
    if (x == 'Bekerja') {
      document.getElementById("demo2").innerHTML='<div class="col-md-12">'+
      '<div class="form-group">'+
      '<label >Nama Perusahaan</label>'+
      '<input class="form-control" type="text" name="fam_per[]" onkeyup="this.value = this.value.toUpperCase()">'+
      '<div class="help-block form-text text-muted form-control-feedback"></div>'+
      '</div>'+
      '</div>';
    } else {
      document.getElementById("demo2").innerHTML='';
    }

  }
</script>
<script type="text/javascript">

    function myFunction3(e) {
      var x = document.getElementById("mySelect3").value;
      if (x == 'Masih Hidup') {
        document.getElementById("demo3").innerHTML='<div class="col-md-12">'+
        '<div class="form-group">'+
        '<label >No Hape Ibu</label>'+
        '<input class="form-control" maxlength="16" onclick="myNumberIbu()" id="hp_ibu" type="text" name="fam_no_hp[]" placeholder="No Hape Ibu" data-mask="0000-0000-0000-0">'+
        '<div class="help-block form-text text-muted form-control-feedback"></div>'+
        '</div>'+
        '</div>'+
        '<div class="col-md-12">'+
        '<div class="form-group">'+
        '<label >Usia</label>'+
        '<input class="form-control" type="text" maxlength="3" name="fam_usia[]" onkeypress="return angka(event)" >'+
        '<div class="help-block form-text text-muted form-control-feedback">'+
        '</div>'+
        '</div>'+
        '</div>'+
        '<div class="col-md-12">'+
        '<div class="form-group">'+
        '<label >Pendidikan</label>'+
        '<select name="fam_pen[]" class="form-control">'+
        '<option>Pilih..</option>'+
        '<?php foreach ($pendidikan as $key => $value) { ?>'+
        '<option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>'+
        '<?php } ?>'+
        '</select>'+
        '<div class="help-block form-text text-muted form-control-feedback">'+
        '</div>'+
        '</div>'+
        '</div>'+
        '<div class="col-md-12">'+
        '<div class="form-group">'+
        '<label >Pekerjaan</label>'+
        '<select name="fam_job[]" class="form-control" id="mySelect4" onchange="myFunction4(event)" >'+
        '<option>Pilih..</option>'+
        '<option value="Bekerja">Bekerja</option>'+
        '<option value="Tidak Bekerja">Tidak Bekerja</option>'+
        '</select>'+
        '<div class="help-block form-text text-muted form-control-feedback">'+
        '</div>'+
        '</div>'+
        '</div>';
    } else {
        document.getElementById("demo3").innerHTML='';
    }

}
function myFunction4(e) {
    var x = document.getElementById("mySelect4").value;
  if (x == 'Bekerja') {
    document.getElementById("demo4").innerHTML='<div class="col-md-12">'+
    '<div class="form-group">'+
    '<label >Nama Perusahaan</label>'+
    '<input class="form-control" type="text" name="fam_per[]" onkeyup="this.value = this.value.toUpperCase()">'+
    '<div class="help-block form-text text-muted form-control-feedback"></div>'+
    '</div>'+
    '</div>';
} else {
    document.getElementById("demo4").innerHTML='';
}

}

</script>
<script>
function myNumber() {
  document.getElementById("hp_ayah").value = "08";
}
function myNumberIbu() {
  document.getElementById("hp_ibu").value = "08";
}
</script>