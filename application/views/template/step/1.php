<style type="text/css">
	.select2-container--default .select2-selection--single {
		background-color: #fff;
		border: 1px solid #0000ff;
		border-top: none;
		border-left: none;
		border-right: none;
		border-radius: 0px;
	}
	.wrapper .main .content .content-box .form-wizard-nav .step:before, .wrapper .main .content .media-wrapper .media-row .media-box .form-wizard-nav .step:before, .wrapper .main .content .invoice-wrapper .form-wizard-nav .step:before{
		background-color: #0275d8;
	}
	.form-control{border:none;border-bottom: 1px solid blue;border-radius:0px;}
	label{color:#1e88e5;margin-bottom:-10px;font-size: 15px}
	.form-group{margin-bottom:30px; margin-top: 15px;}
	::-webkit-input-placeholder { /* Edge */
		color: #a2a5a7;
	}

	:-ms-input-placeholder { /* Internet Explorer 10-11 */
		color: #a2a5a7;
	}

	::placeholder {
		color: #a2a5a7 !important;
	}
	.responsive-height{ min-height: 480px; margin-bottom: -10px;}
	@media screen and (max-width: 700px)
	{
		.responsive-height{ min-height: 400px; }
	}
	@media screen and (max-width: 800px)
	{
		.responsive-height{ min-height: 500px; }
	}

	.jarak_kiri{ padding-left:40px; }
	.jarak_kanan{ padding-right:40px; }
	@media screen and (max-width: 575px)
	{
		.jarak_kiri{ padding-left:0px;padding-right:0px; }
		.jarak_kanan{ padding-left:0px;padding-right:0px; }
	}

</style>
<script src="<?= base_url();?>template/datepicker.min.js"></script>
<script src="<?= base_url();?>template/i18n/datepicker.en.js"></script>
<div class="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;padding-bottom: 0px;">
	<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
			<div class="col-md-12">
				<div class="form-wizard-nav">
					<?php for ($i=1; $i <=max_step(); $i++) { ?>
						<?php if ($i<=$no){ ?>
							<div class="step complete active" data-form="#form-<?php echo $i; ?>"></div>
						<?php }else{ ?>
							<div class="step" data-form="#form-<?php echo $i; ?>"></div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
			<div class="col-md-12" id="form-1">
				<form id="form-step-1">
					<div class="row">
						<div class="col-sm-6 jarak_kanan">
							<div class="col-md-12">
								<div class="form-group">
									<label >Nama Lengkap</label>
									<input class="form-control" type="text" name="fullname" value="<?php echo $baris['fullname'];?>" onkeypress="return harusHuruf(event)" required="required">
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >No KTP</label>
									<input class="form-control" type="text" id="no_ktp" minlength="19" maxlength="19" name="no_ktp" value="<?php echo $baris['no_ktp'];?>" onkeypress="return angka(event)" pattern=".{0}|.{19,19}" required="required">
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Tempat Lahir</label>
									<select id="placeholder" name="place_birth" class="form-control select2 required" required>
									<option selected ></option>
									<?php foreach($kota as $cat){
						              $id         	= $cat->id;
						              $distrik      = $cat->distrik;
						              $kota         = $cat->kota;?>
						             <option <?php if($id == $baris['place_birth']){ echo 'selected="selected"'; } ?> value="<?php echo $id; ?>"><?php echo $kota;?>&nbsp; - &nbsp;<?php echo $distrik;?></option>
						          	<?php } ?>
									</select>
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Tanggal Lahir</label>
									<input id="datepicker" class="form-control" placeholder="tgl/bln/tahun" type="text" name="date_birth" value="<?php echo $baris['date_birth'];?>" required="required">
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Jenis Kelamin</label>
									<div class="row">
										<div class="col-sm-6" style="top: 10px">
											<div class="form-group">
												<input <?php $jenis_kelamin = 'PRIA'; if($jenis_kelamin==$baris['jenis_kelamin']){ echo 'checked=""'; } ?> name="jenis_kelamin" type="radio" value="PRIA" required> PRIA
											</div>
										</div>
										<div class="col-sm-6" style="top: 10px">
											<div class="form-group">
												<input <?php $jenis_kelamin = 'WANITA'; if($jenis_kelamin==$baris['jenis_kelamin']){ echo 'checked=""'; } ?> name="jenis_kelamin" type="radio" value=" WANITA" required> WANITA
											</div>
										</div>
									</div>
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label >Alamat</label>
									<input onkeyup="this.value = this.value.toUpperCase()" class="form-control" type="text" name="address" value="<?php echo $baris['address'];?>" required="required">
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
							</div>
						</div>

						<div class="col-sm-6 jarak_kiri">
							<div class="col-md-12">
								<div class="form-group">
									<label >No Handphone</label>
									<input class="form-control" type="text" name="hp_1" value="<?php echo $baris['hp_1'];?>" readonly>
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
								<div class="form-group">
									<label >No Handphone II</label>
									<input class="form-control" id="no_hp" type="text" name="hp_2" value="<?php echo $baris['hp_2'];?>" placeholder="08" maxlength="16"onkeypress="return angka(event)" >
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
								<div class="form-group">
									<label >Agama</label>
									<select placeholder="Pilih" class="form-control" name="religion" required>
										<option></option>
										<option <?php $agama = 'ISLAM'; if ($agama==$baris['religion']) {echo 'selected="selected"';} ?> value="ISLAM">ISLAM</option>
										<option <?php $agama = 'KRISTEN'; if ($agama==$baris['religion']) {echo 'selected="selected"';} ?> value="KRISTEN">KRISTEN</option>
										<option <?php $agama = 'KATOLIK'; if ($agama==$baris['religion']) {echo 'selected="selected"';} ?> value="KATOLIK">KATOLIK</option>
										<option <?php $agama = 'BUDHA'; if ($agama==$baris['religion']) {echo 'selected="selected"';} ?> value="BUDHA">BUDHA</option>
										<option <?php $agama = 'KHONG HUCU'; if ($agama==$baris['religion']) {echo 'selected="selected"';} ?> value="KHONG HUCU">KHONG HUCU</option>
									</select>
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
								<div class="form-group">
									<label >Status Pernikahan</label>
									<select class="form-control" id="marital" name="marital" required>
										<option></option>
										<?php foreach($marital as $cat){
											$id         = $cat->id;
											$title      = $cat->keterangan;?>
											<option <?php if($id == $baris['marital']){ echo 'selected="selected"'; } ?> value="<?php echo $id; ?>"><?php echo $title; ?></option>
										<?php } ?>
									</select>
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
								<div class="form-group">
									<label >Anda Berkacamata</label>
									<div class="row">
										<div class="col-sm-6" style="top: 10px">
											<div class="form-group">
												<input <?php $mata = 'YA'; if($mata==$baris['kacamata']){ echo 'checked=""'; } ?> name="kacamata" type="radio" value="YA" required> YA
											</div>
										</div>
										<div class="col-sm-6" style="top: 10px">
											<div class="form-group">
												<input <?php $mata = 'TIDAK'; if($mata==$baris['kacamata']){ echo 'checked=""'; } ?> name="kacamata" type="radio" value="TIDAK" required> TIDAK
											</div>
										</div>
									</div>
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
								<div class="form-group">
									<label >Hobby</label>
									<input onkeyup="this.value = this.value.toUpperCase()" class="form-control" type="text" name="hobby" value="<?php echo $baris['hobby'];?>" required="required">
									<div class="help-block form-text text-muted form-control-feedback"></div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="content-box-footer">
					<a onclick="step(2)" class="btn btn-success next-action pull-right ">Next</a>
					<!-- <button class="btn btn-primary next-action">Next</button> -->
				</div>
			</div>
	</div>
</div>
<script>
	function harusHuruf(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
			return false;
		return true;
	}

			function angka(evt){
                    var charCode = (evt.which) ? evt.which : event.keyCode
                    if (charCode <33 || charCode > 57) {
                      return false;
                    }
                    return true;
                  }
</script>
<script type="text/javascript">

$(document).ready(function() {
    $('#datepicker').datetimepicker({
         format: 'YYYY-MM-DD',
         minDate: '1565-01-01',
         maxDate: '2004-01-01',
         
        
    })<?php if ($baris['date_birth'] == null) {
    	echo ".val('')";
    } else {

    } ?>


});

</script>
<script type="text/javascript">
	$('#no_hp').click(function(event) {
		var val = $('#no_hp').val();
		if(val==''){
			$('#no_hp').val('08');  
		}
	});
	$('#no_hp').focus(function(event) {
		$('#no_hp').val();
	});

	$("#no_hp").focusout(function(){
		$('#no_hp').val();
	});

	$("#no_hp").keyup(function(){
		var val = $('#no_hp').val();
      //var valx = '085834246342';
      var valx = val.replace(/-/gi, "");
      //console.log(valx);
      arr = valx.match(/.{1,4}/g);

      //bersihkan array jika ada yg undefined
      var i = 0;
      var string = "";
      if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
      	string = arr[0];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
      	string = arr[0]+"-"+arr[1];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
      	string = arr[0]+"-"+arr[1]+"-"+arr[2];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
      	string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
      }

      
      //string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
      //console.log(string);
      $('#no_hp').val('');
      $('#no_hp').val(string);
      if(val=='08'){
      	$('#no_hp').val('08'); 
      }else if(val=='0'){
      	$('#no_hp').val('08'); 
      }else if(val==''){
      	$('#no_hp').val('08'); 
      }
  });
</script>
<script type="text/javascript">
	$("#placeholder").select2({
    placeholder: "Pilih..",
    allowClear: true
});

</script>