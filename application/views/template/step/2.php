<style type="text/css">
	.select2-container--default .select2-selection--single {
		background-color: #fff;
		border: 1px solid #0000ff;
		border-top: none;
		border-left: none;
		border-right: none;
		border-radius: 0px;
	}
	.wrapper .main .content .content-box .form-wizard-nav .step:before, .wrapper .main .content .media-wrapper .media-row .media-box .form-wizard-nav .step:before, .wrapper .main .content .invoice-wrapper .form-wizard-nav .step:before{
		background-color: #0275d8;
	}
	.form-control{border:none;border-bottom: 1px solid blue;border-radius:0px;}
	label{color:#1e88e5;margin-bottom:-10px;font-size: 15px}
	.form-group{margin-bottom:30px; margin-top: 15px;}
	::-webkit-input-placeholder { /* Edge */
		color: #a2a5a7;
	}

	:-ms-input-placeholder { /* Internet Explorer 10-11 */
		color: #a2a5a7;
	}

	::placeholder {
		color: #a2a5a7 !important;
	}
	.responsive-height{ min-height: 480px; margin-bottom: -10px;}
	@media screen and (max-width: 700px)
	{
		.responsive-height{ min-height: 400px; }
	}
	@media screen and (max-width: 800px)
	{
		.responsive-height{ min-height: 500px; }
	}

	.jarak_kiri{ padding-left:40px; }
	.jarak_kanan{ padding-right:40px; }
	@media screen and (max-width: 575px)
	{
		.jarak_kiri{ padding-left:0px;padding-right:0px; }
		.jarak_kanan{ padding-left:0px;padding-right:0px; }
	}

</style>
<script src="<?= base_url();?>template/datepicker.min.js"></script>
<script src="<?= base_url();?>template/i18n/datepicker.en.js"></script>
<div class="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;padding-bottom: 0px;">
	<div class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
		<div class="col-md-12">
			<div class="form-wizard-nav">
				<?php for ($i=1; $i <=max_step(); $i++) { ?>
					<?php if ($i<=$no){ ?>
						<div class="step complete active" data-form="#form-<?php echo $i; ?>"></div>
					<?php }else{ ?>
						<div class="step" data-form="#form-<?php echo $i; ?>"></div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
		<div class="col-md-12" id="form-2">
			<form id="form-step-1">
				<div class="row">
					<div class="col-sm-6 jarak_kanan">
						<div class="col-md-12">
							<div class="form-group">
								<label >Gol. Darah</label>
								<select class="form-control" name="darah" name="darah" required="required">
									<option></option>
									<option <?php $darah='O';if($darah==$baris['darah']){echo 'selected="selected"';} ?> value="O">O</option>
									<option <?php $darah='A';if($darah==$baris['darah']){echo 'selected="selected"';} ?> value="A">A</option>
									<option <?php $darah='B';if($darah==$baris['darah']){echo 'selected="selected"';} ?> value="B">B</option>
									<option <?php $darah='AB';if($darah==$baris['darah']){echo 'selected="selected"';} ?> value="AB">AB</option>
									<option <?php $darah='Tidak Tahu';if($darah==$baris['darah']){echo 'selected="selected"';} ?> value="Tidak Tahu">Tidak Tahu</option>
								</select>
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Email</label>
								<input class="form-control" type="email" name="email"  value="<?php echo $baris['email'];?>" required="required">
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Telegram</label>
								<input class="form-control" type="text" name="telegram"  value="<?php echo $baris['telegram'];?>" required="required">
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Facebook</label>
								<input class="form-control" type="text" name="facebook" value="<?php echo $baris['facebook'];?>" required="required">
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Twitter</label>
								<input class="form-control" type="text" name="twitter"  value="<?php echo $baris['twitter'];?>" required="required">
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Instagram</label>
								<input onkeyup="this.value = this.value.toUpperCase()" class="form-control" type="text" name="instagram" value="<?php echo $baris['instagram'];?>" required="required">
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
					</div>

					<div class="col-sm-6 jarak_kiri">
						<div class="col-md-12">
							<div class="form-group">
								<label>Kenderaan Yang Di Miliki</label>
								<select class="form-control" name="kendaraan" required="required">
									<option></option>
									<option<?php $kendaraan ='Tidak Punya';if($kendaraan==$baris['kendaraan']){echo' selected="selected"';}?> value="Tidak Punya">Tidak Punya</option>
									<option<?php $kendaraan ='Motor';if($kendaraan==$baris['kendaraan']){echo' selected="selected"';}?> value="Motor">Motor</option>
									<option<?php $kendaraan ='Mobil';if($kendaraan==$baris['kendaraan']){echo' selected="selected"';}?> value="Mobil">Mobil</option>
									<option<?php $kendaraan ='Motor dan Mobil';if($kendaraan==$baris['kendaraan']){echo' selected="selected"';}?> value="Motor dan Mobil">Motor dan Mobil</option>
								</select>
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Apaka Anda Mempunyai Saudara Kandung ??!!</label>
								<select class="form-control" name="" required="required">
									<option>Pilih</option>
									<option>YA</option>
									<option>TIDAK</option>
								</select>
								<div class="help-block form-text text-muted form-control-feedback"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div id="my_photo_booth">
	<div id="my_camera"></div>
	<!-- First, include the Webcam.js JavaScript Library -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/webcamjs-master/webcam.min.js"></script>
	<!-- Configure a few settings and attach camera -->
	<script language="JavaScript">
		Webcam.set({
				// live preview size
				width: 300,
				height: 200,
				
				// device capture size
				dest_width: 600,
				dest_height: 400,
				
				// final cropped size
				crop_width: 400,
				crop_height: 400,
				
				// format and quality
				image_format: 'jpeg',
				jpeg_quality: 200,
				
				// flip horizontal (mirror mode)
				flip_horiz: true
			});
		Webcam.attach( '#my_camera' );
	</script>
	<!-- A button for taking snaps -->
	<!-- <form> -->
		<div id="pre_take_buttons">
			<!-- This button is shown before the user takes a snapshot -->
			<input type=button value="Take Snapshot" onClick="preview_snapshot()">
		</div>
		<div id="post_take_buttons" style="display:none">
			<!-- These buttons are shown after a snapshot is taken -->
			<input type=button value="&lt; Take Another" onClick="cancel_preview()">
			<input type=button value="Save Photo &gt;" onClick="save_photo()" style="font-weight:bold;">
		</div>
		<!-- </form> -->
	</div>
	<div id="results" style="display:none">
		<!-- Your captured image will appear here... -->
	</div>
	<!-- Code to handle taking the snapshot and displaying it locally -->
	<script language="JavaScript">
		// preload shutter audio clip
		var shutter = new Audio();
		shutter.autoplay = false;
		shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';
		
		function preview_snapshot() {
			// play sound effect
			try { shutter.currentTime = 0; } catch(e) {;} // fails in IE
			shutter.play();
			
			// freeze camera so user can preview current frame
			Webcam.freeze();
			
			// swap button sets
			document.getElementById('pre_take_buttons').style.display = 'none';
			document.getElementById('post_take_buttons').style.display = '';
		}
		function cancel_preview() {
			// cancel preview freeze and return to live camera view
			Webcam.unfreeze();
			
			// swap buttons back to first set
			document.getElementById('pre_take_buttons').style.display = '';
			document.getElementById('post_take_buttons').style.display = 'none';
		}
		function save_photo() {
			// actually snap photo (from preview freeze) and display it
			Webcam.snap( function(data_uri) {
				// display results in page
				document.getElementById('results').innerHTML = 
				'<h2>Here is your large, cropped image:</h2>' + 
				'<img src="'+data_uri+'"/><br/></br>' + 
				'<input type="file" value="'+data_uri+'" name="foto">';
				
				Webcam.upload( data_uri, 'upload.php', function(code, text) {} );

                // tampilkan hasil gambar yang telah di ambil
                document.getElementById('hasil').innerHTML = 
                '<p>Hasil : </p>' + 
                '<img src="'+data_uri+'"/>';
				// shut down camera, stop capturing
				Webcam.reset();
				
				// show results, hide photo booth
				document.getElementById('results').style.display = '';
				document.getElementById('my_photo_booth').style.display = 'none';

			});

		}

	</script>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
	<div class="content">
			<div class="content-box-footer" style="position: relative;">
				<a onclick="step(1,'prev')" class="btn btn-warning prev-action">Back</a>
				<a onclick="step(3,'next')" class="btn btn-success next-action pull-right">Next</a>
			</div>
	</div>
<script>
	function harusHuruf(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
			return false;
		return true;
	}

	function angka(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode <33 || charCode > 57) {
			return false;
		}
		return true;
	}
</script>