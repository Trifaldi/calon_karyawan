<div class="content">
	<div class="panel">
		<div class="row">
			<div class="col-md-12">
				<div class="content-box">
					<div class="row" id="smartwizard" >
						<div class="col-md-12">
							<div class="form-wizard-nav">
								<div class="step complete active" data-form="#form-1">Step 1</div>
								<div class="step complete active" data-form="#form-2">Step 2</div>
								<div class="step" data-form="#form-3">Step 3</div>
								<div class="step" data-form="#form-4">Step 4</div>
								<div class="step" data-form="#form-5">Step 5</div>
							</div>
						</div>
						<div class="col-md-12" id="form-1">
							<form>
								<div class="form-group row">
									<label class="col-sm-2 control-label">Email</label>
									<div class="col-sm-10">
										<input class="form-control" placeholder="Enter email" type="email">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-2 control-label">Email</label>
									<div class="col-sm-10">
										<input class="form-control" placeholder="Enter email" type="email">
									</div>
								</div>
							</form>
							<div class="content-box-footer">
								<a href="<?= base_url(); ?>welcome" class="btn btn-primary prev-action">Prev</a>
								<button class="btn btn-primary next-action">Next</button>
							</div>
						</div>
						<div class="col-md-12" id="form-3">
							<form>
								<div class="form-group row">
									<label class="col-sm-2 control-label">Password</label>
									<div class="col-sm-10">
										<input class="form-control" placeholder="Enter password" type="password">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-2 control-label">Confirm Password</label>
									<div class="col-sm-10">
										<input class="form-control" placeholder="Enter confirm password" type="password">
									</div>
								</div>
							</form>
							<div class="content-box-footer">
								<button class="btn btn-primary prev-action">Prev</button>
								<button class="btn btn-primary next-action">Next</button>
							</div>
						</div>
						<div class="col-md-12" id="form-4">
							<form>
								<div class="form-group row">
									<label class="col-sm-2 control-label">Phone Number</label>
									<div class="col-sm-10">
										<input class="form-control" placeholder="Enter phone number" type="text">
									</div>
								</div>
							</form>
							<div class="content-box-footer">
								<button class="btn btn-primary prev-action">Prev</button>
								<button class="btn btn-primary next-action">Next</button>
							</div>
						</div>
						<div class="col-md-12" id="form-5">
							<form>
								<div class="form-group row">
									<label class="col-sm-2 control-label">Address</label>
									<div class="col-sm-10">
										<input class="form-control" placeholder="Enter address" type="text">
									</div>
								</div>
							</form>
							<div class="content-box-footer">
								<button class="btn btn-primary prev-action">Prev</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#smartwizard').smartWizard();
});
</script>