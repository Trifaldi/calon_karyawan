<style type="text/css">
	.select2-container--default .select2-selection--single {
		background-color: #fff;
		border: 1px solid #0000ff;
		border-top: none;
		border-left: none;
		border-right: none;
		border-radius: 0px;
	}
	.wrapper .main .content .content-box .form-wizard-nav .step:before, .wrapper .main .content .media-wrapper .media-row .media-box .form-wizard-nav .step:before, .wrapper .main .content .invoice-wrapper .form-wizard-nav .step:before{
		background-color: #0275d8;
	}
	.form-control{border:none;border-bottom: 1px solid blue;border-radius:0px;}
	label{color:#1e88e5;margin-bottom:-10px;font-size: 15px}
	.form-group{margin-bottom:30px; margin-top: 15px;}
	::-webkit-input-placeholder { /* Edge */
		color: #a2a5a7;
	}

	:-ms-input-placeholder { /* Internet Explorer 10-11 */
		color: #a2a5a7;
	}

	::placeholder {
		color: #a2a5a7 !important;
	}
	.responsive-height{ min-height: 480px; margin-bottom: -10px;}
	@media screen and (max-width: 700px)
	{
		.responsive-height{ min-height: 400px; }
	}
	@media screen and (max-width: 800px)
	{
		.responsive-height{ min-height: 500px; }
	}

	.jarak_kiri{ padding-left:40px; }
	.jarak_kanan{ padding-right:40px; }
	@media screen and (max-width: 575px)
	{
		.jarak_kiri{ padding-left:0px;padding-right:0px; }
		.jarak_kanan{ padding-left:0px;padding-right:0px; }
	}

</style>
<script src="<?= base_url();?>template/datepicker.min.js"></script>
<script src="<?= base_url();?>template/i18n/datepicker.en.js"></script>
<?php $max_step=6; ?>
<div class="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;padding-bottom: 0px;">
	<div class="content-box" class="content-box" style="margin-top: 0px;padding-top: 0px;padding-bottom: 48px;padding-left: 10px;padding-right: 10px;">
		<div class="col-md-12">
			<div class="form-wizard-nav">
				<?php for ($i=1; $i <=$max_step; $i++) {?>
					<div class="step" data-form="#form-<?php echo $i; ?>"></div>
				<?php } ?>
			</div>
		</div>
		
		<div class="tuyul" hidden>1</div>
		<?php for ($i=1; $i <=$max_step; $i++) { ?>
			<div class="col-md-12" id="form-<?php echo $i; ?>" style="display: block;padding-left: 50px;padding-right: 50px;">
				<?php $this->load->view('dashboard/step/'.$i.'/form'); ?>
			</div>
		<?php } ?>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {

    // var navListItems = $('div.setup-panel div a'),
    //         allWells = $('.setup-content'),
    //         allNextBtn = $('.nextBtn');
    navListItems = $('.next-action');
    // allWells.hide();
    $('.prev-action').click(function(e){
    	$('.tuyul').html(parseInt($('.tuyul').html())-1);
    	if (!this.hasAttribute('tabindex')) {
    // Choose one of the following lines (but not both):
    this.setAttribute('tabindex', 0);
    this.tabIndex = 0;
}
});

    navListItems.click(function (e) {
        // e.preventDefault();
        var no1 = parseInt($('.tuyul').html());
        var no2 = no1+1;
        var sum_req = 0;
        if($('#form-'+no1+':visible').length == 0){
        	$('#form-'+no1+' :required').each(function(){
        		if($(this).prop('required')){
        			if ($(this).val()=='') {
        				sum_req+=1; 	
				   //console.log($(this).val());
				}			       
			}
		});

        	if ($('#no_ktp').val().length < 19) {
        		sum_req += 1;
        	}else if ($('#email').val().length == '') {
        		sum_req = 0;
        	}

        	console.log(sum_req);
        	if(sum_req > 0){
        		$('.tuyul').html(no1);
        		$('.form-wizard-nav > .step[data-form="#form-'+no1+'"]').addClass('complete active');
        		$('.form-wizard-nav > .step[data-form="#form-'+no2+'"]').removeClass('complete active');
        		$('#form-'+no1+'').show();
        		$('#form-'+no2+'').hide();
        	}else{
        		$('.tuyul').html(no2);
        		$('.form-wizard-nav > .step[data-form="#form-'+no1+'"]').removeClass('active');
        		$('.form-wizard-nav > .step[data-form="#form-'+no2+'"]').addClass('complete active');
        		$('#form-'+no1+'').hide();
        		$('#form-'+no2+'').show();
        	}
        }
    });
});
</script>
    <script type="text/javascript">
    	$('#no_hp').click(function(event) {
    		var val = $('#no_hp').val();
    		if(val==''){
    			$('#no_hp').val('08');  
    		}
    	});
    	$('#no_hp').focus(function(event) {
    		$('#no_hp').val();
    	});

    	$("#no_hp").focusout(function(){
    		$('#no_hp').val();
    	});

    	$("#no_hp").keyup(function(){
    		var val = $('#no_hp').val();
  //var valx = '085834246342';
  var valx = val.replace(/-/gi, "");
  console.log(valx);
  arr = valx.match(/.{1,4}/g);

  //bersihkan array jika ada yg undefined
  var i = 0;
  var string = "";
  if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
  	string = arr[0];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
  	string = arr[0]+"-"+arr[1];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
  	string = arr[0]+"-"+arr[1]+"-"+arr[2];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
  	string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
  }

  
  //string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
  console.log(string);
  $('#no_hp').val('');
  $('#no_hp').val(string);
  if(val=='08'){
  	$('#no_hp').val('08'); 
  }else if(val=='0'){
  	$('#no_hp').val('08'); 
  }else if(val==''){
  	$('#no_hp').val('08'); 
  }
});
</script>
<!-- <script type="text/javascript">
  $(document).ready(function() {
    $("#form-user5").hide();
      $("#anak").hide();
        $('#marital').on('change', function() {
          var id = $(this).val();
          if (id == 1 ) {
            //$('p').remove()
            $("#form-user5").show();
            $("##anak").hide();
          } else {
            //$('#p').remove()
            $("#anak").show();
            $("#form-user5").hide();
          }
        });
    });
</script> -->