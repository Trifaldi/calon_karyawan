<form class="responsive-height" name="myForm" action="javascript:void(0)" method="POST" id="form-user5">
	<div class="row">
		<div class="col-sm-6 jarak_kanan">
			<?php $this->load->view('dashboard/step/5/left'); ?>
		</div>
		<div class="col-sm-6 jarak_kiri">
			<?php $this->load->view('dashboard/step/5/right'); ?>
		</div>
	</div>
	<div class="content-box-footer">
		<button class="btn btn-warning prev-action" style="color: black;">Back</button>
		<?php if ($personal->proses_status == 1) { ?>
			<button id="tombol-simpan5" class="tombol-simpan5a btn btn-success pull-right next-action">Next</button>
		<?php }else { ?>
			<button id="tombol-simpan5" class="tombol-simpan5 btn btn-success pull-right next-action">Next</button>
		<?php } ?>
		
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$(".tombol-simpan5").click(function(){
			var data = $('#form-user5').serialize();
			$.ajax({
				type: 'POST',
				url: "<?php echo base_url('beranda/create5')?>",
				data: data,
				async: false,
				dataType: 'json',
				success: function(response) {
					//console.log(data);
				}
			});
		});

		$(".tombol-simpan5a").click(function(){
			var data = $('#form-user5').serialize();
			$.ajax({
				type: 'POST',
				url: "<?php echo base_url('beranda/create5a')?>",
				data: data,
				async: false,
				dataType: 'json',
				success: function(response) {
					//console.log(data);
				}
			});
		});

	});
</script>