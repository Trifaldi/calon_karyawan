<form class="responsive-height" name="myForm" action="javascript:void(0)" method="POST" id="form-user2">
	<div class="row">
		<div class="col-sm-6 jarak_kanan">
			<?php $this->load->view('dashboard/step/2/left'); ?>
		</div>
		<div class="col-sm-6 jarak_kiri">
			<?php $this->load->view('dashboard/step/2/right'); ?>
		</div>
	</div>
	<div class="content-box-footer">
		<button class="btn btn-warning prev-action" style="color: black;">Back</button>
		<button id="tombol-simpan2" class="tombol-simpan2 btn btn-success pull-right next-action">Next</button>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$(".tombol-simpan2").click(function(){
			Webcam.snap( function(data_uri) {
				Webcam.upload( data_uri, 'beranda/create2', function(code, text) {
					var data = $('#form-user2').serialize();
					$.ajax({
						type: 'POST',
						url: "<?php echo base_url('beranda/create2')?>",
						data: data,
						async: false,
						dataType: 'json',
						success: function(response) {
							//console.log(data);
						}
					});
				} );
                // tampilkan hasil gambar yang telah di ambil
                Webcam.unfreeze();
            } );
		});
	});
</script>