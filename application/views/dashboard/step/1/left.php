
<?php  
//$nama['username'] = $this->session->userdata('nama');
$data[] = array('type'=>'text', 'name'=>'fullname', 'judul'=>'Nama Lengkap', 'html'=>'required');
$data[] = array('type'=>'text', 'name'=>'no_ktp', 'judul'=>'No_KTP', 'html'=>' id="no_ktp" minlength="19" maxlength="19"  value="'.$personal->no_ktp.'" onkeypress="return angka(event)" pattern=".{0}|.{19,19}" required ');
$data[] = array('type'=>'', 'name'=>'place_birth', 'judul'=>'Tempat Lahir', 'html'=>'');
$data[] = array('type'=>'text', 'name'=>'date_birth', 'judul'=>'Tanggal Lahir', 'html'=>' placeholder="tgl/bln/tahun" id="datepicker" value="'.$personal->date_birth.'" required');
$data[] = array('type'=>'text', 'name'=>'jenis_kelamin', 'judul'=>'Jenis Kelamin', 'html'=>'');
$data[] = array('type'=>'text', 'name'=>'address', 'judul'=>'Alamat', 'html'=>'onkeyup="this.value = this.value.toUpperCase()" value="'.$personal->address.'" required');
?>

<?php 
$i=1;
foreach ($data as $key => $value) { ?>
	<?php if ($i==1){ ?>
	<div class="col-md-12">
		<div class="form-group">
			<label for="yourname"><?php echo $value['judul']; ?></label>
				<input class="form-control" id="nama" type="text" name="<?php echo $value['name']; ?>" value="<?php echo $this->session->userdata('fullname') ?>" onkeyup="this.value = this.value.toUpperCase()"  onkeypress="return harusHuruf(event)" >
			<div class="help-block form-text text-muted form-control-feedback"></div>
		</div>
	</div>
	<?php } else if($i==3){ ?>

	<div class="col-md-12">
		<div class="form-group">
			<label style="margin-bottom: 0px"><?php echo $value['judul']; ?></label>
			<select id="placeholder" class="form-control select2 required" name="<?php echo $value['name'];?>" required>
					<option selected disabled ></option>
					<?php foreach($kota as $cat){
              $id         = $cat->id;
              $distrik         = $cat->distrik;
              $kota         = $cat->kota;?>
              <option <?php if($id == $personal->place_birth){ echo 'selected="selected"'; } ?> value="<?php echo $id; ?>"><?php echo $kota;?>&nbsp; - &nbsp;<?php echo $distrik;?></option>
          <?php } ?>
				</select>
			<div class="help-block form-text text-muted form-control-feedback"></div>
		</div>
	</div>
	<?php }else if($i==5){ ?>
	<div class="col-md-12">
		<div class="form-group">
			<label><?php echo $value['judul']; ?></label>
			<div class="row">
				<div class="col-sm-6" style="top: 10px">
					<div class="form-group">
						<input <?php $jenis_kelamin = 'PRIA'; if($jenis_kelamin==$personal->jenis_kelamin){ echo 'checked=""'; } ?> name="<?php echo $value['name']; ?>" type="radio" value="PRIA" required> PRIA
					</div>
				</div>
				<div class="col-sm-6" style="top: 10px">
					<div class="form-group">
						<input <?php $jenis_kelamin = 'WANITA'; if($jenis_kelamin==$personal->jenis_kelamin){ echo 'checked=""'; } ?> name="<?php echo $value['name']; ?>" type="radio" value=" WANITA" required> WANITA
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } else { ?>
		<div class="col-md-12">
			<div class="form-group">
				<label ><?php echo $value['judul']; ?></label>
				<input class="form-control" type="<?php echo $value['type']; ?>" name="<?php echo $value['name'];?>"<?php echo $value['html'] ?>>
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
	<?php } ?>
<?php 
$i++;
} ?>
<script>

 //  Bind the event handler to the "submit" JavaScript event
$('.yourname').click(function(){

    // Get the Login Name value and trim it
    var name = $('#yourname').val();

    // Check if empty of not
    if (name.length < 1) {
        alert('Text-field is empty.');
        return false;
    }

});

</script>
<script>
	function harusHuruf(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
			return false;
		return true;
	}

			function angka(evt){
                    var charCode = (evt.which) ? evt.which : event.keyCode
                    if (charCode <33 || charCode > 57) {
                      return false;
                    }
                    return true;
                  }
</script>
<!-- <script src="<?php  echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script> -->
<script type="text/javascript">

$(document).ready(function() {
    $('#datepicker').datetimepicker({
         format: 'YYYY-MM-DD',
         minDate: '1565-01-01',
         maxDate: '2004-01-01',
         
        
    })<?php if ($personal->date_birth == null) {
    	echo ".val('')";
    } else {

    } ?>


});

</script>
<script type="text/javascript">
    $('#no_ktp').click(function(event) {
      var val = $('#no_ktp').val();
      if(val==''){
        $('#no_ktp').val();  
      }
      
    });

    $('#no_ktp').focus(function(event) {
      $('#no_ktp').val();
    });

    $("#no_ktp").focusout(function(){
        $('#no_ktp').val();
    });

    $("#no_ktp").keyup(function(){
      var val = $('#no_ktp').val();
      //var valx = '085834246342';
      var valx = val.replace(/-/gi, "");
      console.log(valx);
      arr = valx.match(/.{1,4}/g);

      //bersihkan array jika ada yg undefined
      var i = 0;
      var string = "";
      if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
        string = arr[0];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
        string = arr[0]+"-"+arr[1];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
        string = arr[0]+"-"+arr[1]+"-"+arr[2];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
        string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
      }

     
      //string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
      console.log(string);
      $('#no_ktp').val('');
      $('#no_ktp').val(string);


    });

  </script>
<script type="text/javascript">
	$("#placeholder").select2({
    placeholder: "Pilih..",
    allowClear: true
});

</script>