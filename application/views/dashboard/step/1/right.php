
<?php  
//$nama['username'] = $this->session->userdata('nama');
$data[] = array('type'=>'text', 'name'=>'hp_1', 'judul'=>'No Handpone', 'html'=>'');
$data[] = array('type'=>'text', 'name'=>'hp_2', 'judul'=>'No Handpone II', 'html'=>'id="no_hp" placeholder="08" maxlength="16"onkeypress="return angka(event)" value="'.$personal->hp_2.'"');
$data[] = array('type'=>'', 'name'=>'religion', 'judul'=>'Agama', 'html'=>'');
$data[] = array('type'=>'', 'name'=>'marital', 'judul'=>'Status Pernikahan');
$data[] = array('type'=>'text', 'name'=>'kacamata', 'judul'=>'Anda berkacamata ', 'html'=>'required');
$data[] = array('type'=>'text', 'name'=>'hobby', 'judul'=>'Hobby', 'html'=>'onkeyup="this.value = this.value.toUpperCase()" value="'.$personal->hobby.'" required');
?>

<?php 
$i=1;
foreach ($data as $key => $value) { ?>
	<?php if ($i==3){ ?>
		<div class="col-md-12">
			<div class="form-group">
				<label><?php echo $value['judul']; ?></label>
				<select placeholder="Pilih" class="form-control" name="<?php echo $value['name'];?>" required>
					<option></option>
					<option <?php $agama = 'ISLAM'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="ISLAM">ISLAM</option>
					<option <?php $agama = 'KRISTEN'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="KRISTEN">KRISTEN</option>
					<option <?php $agama = 'KATOLIK'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="KATOLIK">KATOLIK</option>
					<option <?php $agama = 'BUDHA'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="BUDHA">BUDHA</option>
					<option <?php $agama = 'KHONG HUCU'; if ($agama==$personal->religion) {echo 'selected="selected"';} ?> value="KHONG HUCU">KHONG HUCU</option>
				</select>
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
	<?php } else if ($i==4){ ?>
		<div class="col-md-12">
			<div class="form-group">
				<label><?php echo $value['judul']; ?></label>
				<select class="form-control" id="martial" name="<?php echo $value['name'];?>" required>
					<option></option>
					<?php foreach($martial as $cat){
						$id         = $cat->id;
						$title      = $cat->keterangan;?>
						<option <?php if($id == $personal->marital){ echo 'selected="selected"'; } ?> value="<?php echo $id; ?>"><?php echo $title; ?></option>
					<?php } ?>
				</select>
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
	<?php } else if($i==5){ ?>
		<div class="col-md-12">
			<div class="form-group">
				<label><?php echo $value['judul']; ?></label>
				<div class="row">
					<div class="col-sm-6" style="top: 10px">
						<div class="form-group">
							<input <?php $mata = 'YA'; if($mata==$personal->kacamata){ echo 'checked=""'; } ?> name="<?php echo $value['name']; ?>" type="radio" value="YA" required> YA
						</div>
					</div>
					<div class="col-sm-6" style="top: 10px">
						<div class="form-group">
							<input <?php $mata = 'TIDAK'; if($mata==$personal->kacamata){ echo 'checked=""'; } ?> name="<?php echo $value['name']; ?>" type="radio" value="TIDAK" required> TIDAK
						</div>
					</div>
				</div>
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
	<?php }else if($i==1){ ?>
		<div class="col-md-12">
			<div class="form-group">
				<label><?php echo $value['judul']; ?></label>
				<input class="form-control"  type="text" name="<?php echo $value['name']; ?>" value="<?php echo $this->session->userdata('hp_1') ?> " readonly>
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
	<?php } else { ?>
		<div class="col-md-12">
			<div class="form-group">
				<label><?php echo $value['judul']; ?></label>
				<input class="form-control" type="<?php echo $value['type']; ?>" name="<?php echo $value['name']; ?>" placeholder="Input <?php echo $value['judul']; ?>" <?php echo $value['html'] ?>>
				<div class="help-block form-text text-muted form-control-feedback"></div>
			</div>
		</div>
	<?php } ?>
	<?php 
	$i++;
} ?>


<script type="text/javascript">
	$('#no_hp').click(function(event) {
		var val = $('#no_hp').val();
		if(val==''){
			$('#no_hp').val('08');  
		}
	});
	$('#no_hp').focus(function(event) {
		$('#no_hp').val();
	});

	$("#no_hp").focusout(function(){
		$('#no_hp').val();
	});

	$("#no_hp").keyup(function(){
		var val = $('#no_hp').val();
      //var valx = '085834246342';
      var valx = val.replace(/-/gi, "");
      //console.log(valx);
      arr = valx.match(/.{1,4}/g);

      //bersihkan array jika ada yg undefined
      var i = 0;
      var string = "";
      if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
      	string = arr[0];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
      	string = arr[0]+"-"+arr[1];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
      	string = arr[0]+"-"+arr[1]+"-"+arr[2];
      }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
      	string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
      }

      
      //string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
      //console.log(string);
      $('#no_hp').val('');
      $('#no_hp').val(string);
      if(val=='08'){
      	$('#no_hp').val('08'); 
      }else if(val=='0'){
      	$('#no_hp').val('08'); 
      }else if(val==''){
      	$('#no_hp').val('08'); 
      }
  });
</script>