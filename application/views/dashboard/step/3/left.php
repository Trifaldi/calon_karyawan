<script src="https://bossanova.uk/jsuites/v2/jsuites.js"></script>
<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css" />
<div class="col-md-12">
  <div class="form-group">
    <label >Nama Ayah</label>
    <input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe');?>">
    <input type="hidden" name="fam_ket[]" value="Ayah" >
    <input type="hidden" name="fam_kategori[]" value="1" >
    <input type="hidden" name="fam_jekel[]" value="Pria" >
    <input class="form-control" type="text" name="fam_nama[]" required="required" onkeyup="this.value = this.value.toUpperCase()">
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >Status</label>
    <select name="fam_sttus[]" class="form-control" id="mySelect" onchange="myFunction(event)" >
      <option>Pilih..</option>
      <option value="Masih Hidup">Masih Hidup</option>
      <option value="Almarhum">Almarhum</option>
    </select>
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div id="demo"></div>
<div id="demo2"></div>
<script type="text/javascript">
  function myFunction(e) {
    var x = document.getElementById("mySelect").value;
    if (x == 'Masih Hidup') {
      document.getElementById("demo").innerHTML='<div class="col-md-12">'+
      '<div class="form-group">'+
      '<label >No Hape Ayah</label>'+
      '<input class="form-control" maxlength="16"onclick="myNumber()" id="hp_ayah" type="text" onkeypress="return angka(event)" name="fam_no_hp[]" placeholder="No Hp Ayah" data-mask="0000-0000-0000-0">'+
      '<div class="help-block form-text text-muted form-control-feedback"></div>'+
      '</div>'+
      '</div>'+
      '<div class="col-md-12">'+
      '<div class="form-group">'+
      '<label >Usia</label>'+
      '<input class="form-control" type="text" name="fam_usia[]" maxlength="3" onkeypress="return angka(event)">'+
      '<div class="help-block form-text text-muted form-control-feedback">'+
      '</div>'+
      '</div>'+
      '</div>'+
      '<div class="col-md-12">'+
      '<div class="form-group">'+
      '<label >Pendidikan</label>'+
      '<select name="fam_pen[]" class="form-control">'+
      '<option>Pilih..</option>'+
      '<?php foreach ($pendidikan as $key => $value) { ?>'+
      '<option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>'+
      '<?php } ?>'+
      '</select>'+
      '<div class="help-block form-text text-muted form-control-feedback">'+
      '</div>'+
      '</div>'+
      '</div>'+
      '<div class="col-md-12">'+
      '<div class="form-group">'+
      '<label >Pekerjaan</label>'+
      '<select name="fam_job[]" class="form-control" id="mySelect2" onchange="myFunction2(event)" >'+
      '<option>Pilih..</option>'+
      '<option value="Bekerja">Bekerja</option>'+
      '<option value="Tidak Bekerja">Tidak Bekerja</option>'+
      '</select>'+
      '<div class="help-block form-text text-muted form-control-feedback">'+
      '</div>'+
      '</div>'+
      '</div>';
    } else {
      document.getElementById("demo").innerHTML='';
    }
  }
  function myFunction2(e) {
    var x = document.getElementById("mySelect2").value;
    if (x == 'Bekerja') {
      document.getElementById("demo2").innerHTML='<div class="col-md-12">'+
      '<div class="form-group">'+
      '<label >Nama Perusahaan</label>'+
      '<input class="form-control" type="text" name="fam_per[]" onkeyup="this.value = this.value.toUpperCase()">'+
      '<div class="help-block form-text text-muted form-control-feedback"></div>'+
      '</div>'+
      '</div>';
    } else {
      document.getElementById("demo2").innerHTML='';
    }

  }
</script>