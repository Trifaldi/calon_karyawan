<form class="responsive-height" name="myForm" action="javascript:void(0)" method="POST" id="form-user3">
	<div class="row">
		<div class="col-sm-6 jarak_kanan">
			<?php $this->load->view('dashboard/step/3/left'); ?>
		</div>
		<div class="col-sm-6 jarak_kiri">
			<?php $this->load->view('dashboard/step/3/right'); ?>
		</div>
	</div>
	<div class="content-box-footer">
		<button class="btn btn-warning prev-action" style="color: black;">Back</button>
		<?php if ($personal->proses_status > 0) { ?>
			<button id="tombol-simpan3a" class="tombol-simpan3a btn btn-success pull-right next-action">Next</button>
		<?php }else { ?>
			<button id="tombol-simpan3" class="tombol-simpan3 btn btn-success pull-right next-action">Next</button>
		<?php } ?>
		
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$(".tombol-simpan3").click(function(){
			var data = $('#form-user3').serialize();
			$.ajax({
				type: 'POST',
				url: "<?php echo base_url('beranda/create3')?>",
				data: data,
				async: false,
				dataType: 'json',
				success: function(response) {
					//console.log(data);
				}
			});
		});

		$(".tombol-simpan3a").click(function(){
			var data = $('#form-user3').serialize();
			$.ajax({
				type: 'POST',
				url: "<?php echo base_url('beranda/create3a')?>",
				data: data,
				async: false,
				dataType: 'json',
				success: function(response) {
					//console.log(data);
				}
			});
		});

	});
</script>
<script>
function myNumber() {
  document.getElementById("hp_ayah").value = "08";
}
function myNumberIbu() {
  document.getElementById("hp_ibu").value = "08";
}
</script>
