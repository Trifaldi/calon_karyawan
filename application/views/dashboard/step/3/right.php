<div class="col-md-12">
  <div class="form-group">
    <label >Nama Ibu</label>
    <input type="hidden" name="id_unix[]" value="<?php echo $this->session->userdata('uniqe');?>">
    <input type="hidden" name="fam_ket[]" value="Ibu">
    <input type="hidden" name="fam_kategori[]" value="2">
    <input type="hidden" name="fam_jekel[]" value="Wanita">
    <input class="form-control" type="text" name="fam_nama[]" onkeyup="this.value = this.value.toUpperCase()">
    <div class="help-block form-text text-muted form-control-feedback"></div>
</div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >Status</label>
    <select name="fam_sttus[]" class="form-control" id="mySelect3" onchange="myFunction3(event)" >
      <option>Pilih..</option>
      <option value="Masih Hidup">Masih Hidup</option>
      <option value="Almarhum">Almarhum</option>
  </select>
  <div class="help-block form-text text-muted form-control-feedback"></div>
</div>
</div>
<p id="demo3"></p>
<p id="demo4"></p>


<script type="text/javascript">

    function myFunction3(e) {
      var x = document.getElementById("mySelect3").value;
      if (x == 'Masih Hidup') {
        document.getElementById("demo3").innerHTML='<div class="col-md-12">'+
        '<div class="form-group">'+
        '<label >No Hape Ibu</label>'+
        '<input class="form-control" maxlength="16" onclick="myNumberIbu()" id="hp_ibu" type="text" name="fam_no_hp[]" placeholder="No Hape Ibu" data-mask="0000-0000-0000-0">'+
        '<div class="help-block form-text text-muted form-control-feedback"></div>'+
        '</div>'+
        '</div>'+
        '<div class="col-md-12">'+
        '<div class="form-group">'+
        '<label >Usia</label>'+
        '<input class="form-control" type="text" maxlength="3" name="fam_usia[]" onkeypress="return angka(event)" >'+
        '<div class="help-block form-text text-muted form-control-feedback">'+
        '</div>'+
        '</div>'+
        '</div>'+
        '<div class="col-md-12">'+
        '<div class="form-group">'+
        '<label >Pendidikan</label>'+
        '<select name="fam_pen[]" class="form-control">'+
        '<option>Pilih..</option>'+
        '<?php foreach ($pendidikan as $key => $value) { ?>'+
        '<option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>'+
        '<?php } ?>'+
        '</select>'+
        '<div class="help-block form-text text-muted form-control-feedback">'+
        '</div>'+
        '</div>'+
        '</div>'+
        '<div class="col-md-12">'+
        '<div class="form-group">'+
        '<label >Pekerjaan</label>'+
        '<select name="fam_job[]" class="form-control" id="mySelect4" onchange="myFunction4(event)" >'+
        '<option>Pilih..</option>'+
        '<option value="Bekerja">Bekerja</option>'+
        '<option value="Tidak Bekerja">Tidak Bekerja</option>'+
        '</select>'+
        '<div class="help-block form-text text-muted form-control-feedback">'+
        '</div>'+
        '</div>'+
        '</div>';
    } else {
        document.getElementById("demo3").innerHTML='';
    }

}
function myFunction4(e) {
    var x = document.getElementById("mySelect4").value;
  if (x == 'Bekerja') {
    document.getElementById("demo4").innerHTML='<div class="col-md-12">'+
    '<div class="form-group">'+
    '<label >Nama Perusahaan</label>'+
    '<input class="form-control" type="text" name="fam_per[]" onkeyup="this.value = this.value.toUpperCase()">'+
    '<div class="help-block form-text text-muted form-control-feedback"></div>'+
    '</div>'+
    '</div>';
} else {
    document.getElementById("demo4").innerHTML='';
}

}

</script>

