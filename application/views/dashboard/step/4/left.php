<div class="col-md-12">
  <div class="form-group">
    <label >Nama Saudara</label>
   <!--  <?php if ($personal->uniqe == $family->id_unix): ?> -->
    <!-- <input type="hidden" name="id_fam" value="<?php echo $saudara1->id_fam ?>"> -->
   <!--  <?php endif ?> -->
    <input type="hidden" id="id_unix" name="id_unix" value="<?php echo $this->session->userdata('uniqe'); ?>">
    <input type="hidden" name="fam_kategori" value="4">
    <input type="hidden" id="fam_ket" name="fam_ket" value="Saudara">
    <input class="form-control" type="text" id="fam_nama" name="fam_nama" onkeyup="this.value = this.value.toUpperCase()" onkeypress="return harusHuruf('event')">
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >Jenis Kelamin</label>
    <select name="fam_jekel" class="form-control" id="fam_jekel">
      <option>Pilih..</option>
      <option value="Pria">Pria</option>
      <option value="Wanita">Wanita</option>
    </select>
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >No Hp Saudara</label>
    <input type="text" id="hp_saud1" name="fam_no_hp" id="fam_no_hp" class="form-control" maxlength="16" onkeypress="return angka(event)">
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >Usia</label>
    <input type="text" name="fam_usia" id="fam_usia" class="form-control" maxlength="3">
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >Pendidikan</label>
    <select class="form-control" name="fam_pen" id="fam_pen">
      <option>Pilih..</option>
      <?php foreach ($pendidikan as $key => $value) { ?>
       <option value="<?php echo $value->id_edu ?>"><?php echo $value->name ?></option>
      <?php } ?>
    </select>
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
    <label >Job</label>
    <select class="form-control" onchange="myFunctionSaudara(event)" id="fam_job" name="fam_job">
      <option>Pilih..</option>
      <option value="Bekerja">Bekerja</option>
      <option value="Tidak Bekerja">Tidak Bekerja</option>
    </select>
    <div class="help-block form-text text-muted form-control-feedback"></div>
  </div>
</div>
<p id="suadara"></p>

<button type="button" type="submit" id="btn_save" class="btn btn-primary pull-right">Save</button>
<script type="text/javascript">

function myFunctionSaudara(e) {
  var x = document.getElementById("fam_job").value;
  if (x == 'Bekerja') {
    document.getElementById("suadara").innerHTML='<div class="col-md-12">'+
    '<div class="form-group">'+
    '<label >Nama Perusahaan</label>'+
    '<input class="form-control" type="text" id="fam_per" name="fam_per" onkeyup="this.value = this.value.toUpperCase()">'+
    '<div class="help-block form-text text-muted form-control-feedback"></div>'+
    '</div>'+
    '</div>';
} else {
    document.getElementById("suadara").innerHTML='';
}

}

</script>
<script type="text/javascript">
      $('#hp_saud1').click(function(event) {
        var val = $('#hp_saud1').val();
        if(val==''){
          $('#hp_saud1').val('08');  
        }
      });
      $('#hp_saud1').focus(function(event) {
        $('#hp_saud1').val();
      });

      $("#hp_saud1").focusout(function(){
        $('#hp_saud1').val();
      });

      $("#hp_saud1").keyup(function(){
        var val = $('#hp_saud1').val();
  //var valx = '085834246342';
  var valx = val.replace(/-/gi, "");
  console.log(valx);
  arr = valx.match(/.{1,4}/g);

  //bersihkan array jika ada yg undefined
  var i = 0;
  var string = "";
  if(arr[0]!==undefined && arr[1]==undefined && arr[2]==undefined && arr[3]==undefined){
    string = arr[0];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]==undefined && arr[3]==undefined){
    string = arr[0]+"-"+arr[1];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]==undefined){
    string = arr[0]+"-"+arr[1]+"-"+arr[2];
  }else if(arr[0]!==undefined && arr[1]!==undefined && arr[2]!==undefined && arr[3]!==undefined){
    string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
  }

  
  //string = arr[0]+"-"+arr[1]+"-"+arr[2]+"-"+arr[3];
  console.log(string);
  $('#hp_saud1').val('');
  $('#hp_saud1').val(string);
  if(val=='08'){
    $('#hp_saud1').val('08'); 
  }else if(val=='0'){
    $('#hp_saud1').val('08'); 
  }else if(val==''){
    $('#hp_saud1').val('08'); 
  }
});
</script>