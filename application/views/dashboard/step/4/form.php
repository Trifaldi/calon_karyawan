<form class="responsive-height" action="javascript:void(0)" method="POST" id="form-user4">
	<div class="row">
		<div class="col-sm-6 jarak_kanan">
			<?php $this->load->view('dashboard/step/4/left'); ?>
		</div>
		<div class="col-sm-6 jarak_kiri">
			<?php $this->load->view('dashboard/step/4/right'); ?>
		</div>
	</div>
	<div class="content-box-footer">
		<button class="btn btn-warning prev-action" style="color: black;">Back</button>
		
		<button class="btn btn-success pull-right next-action">Next</button>
		
	</div>
</form>

<script>
function myNumberSaud() {
  document.getElementById("hp_saud2").value = "08";
}

</script>

<script type="text/javascript">
  $(document).ready(function(){
    show_all();

    $('#mydata').dataTable();

    function show_all(){
      $.ajax({
        type : 'ajax',
        url  : '<?php echo base_url();?>saudara/get',
        async : false,
        dataType : 'json',
        success  : function(data){
          var html = '';
          var i;
          for(i=0; i<data.length; i++){
            html += '<tr>'+
                '<td>'+data[i].fam_nama+'</td>'+
                            '<td>'+data[i].fam_no_hp+'</td>'+
                            '<td style="text-align:center;">'+data[i].fam_jekel+'</td>'+
                            '</tr>';  
          }
          $('#show_data').html(html);
        }
      });
    }

    $('#btn_save').click(function(){
      var data = $('#form-user4').serialize();
      
      $.ajax({
        url: '<?php echo base_url('saudara/save');?>',
        type: 'POST',
        async: false,
        dataType: 'JSON',
        data: data,
        success: function(data){
          $('[name="fam_nama"]').val("");
          $('[name="fam_jekel"]').val("");
          $('[name="fam_no_hp"]').val("");
          $('[name="fam_usia"]').val("");
          $('[name="fam_pen"]').val("");
          $('[name="fam_job"]').val("");
          $('[name="fam_per"]').val("");
          show_all();
        }
      });
      return false;
    });


    $('#show_data').on('click','.item_delete',function(){
            var id_fam = $(this).data('id_fam');
            
            $('#Modal_Delete').modal('show');
            $('[name="_delete"]').val(id_fam);
        });

     $('#btn_delete').on('click',function(){
        var id_fam = $('#_delete').val();
        $.ajax({
            type : "POST",
            url  : "<?php echo site_url('saudara/delete')?>",
            dataType : "JSON",
            data : {id_fam:id_fam},
            success: function(data){
                $('[name="_delete"]').val("");
                $('#Modal_Delete').modal('hide');
                show_product();
            }
        });
        return false;
    });


    // $(".tombol-simpan4").click(function(){
    //  var data = $('#form-user4').serialize();
    //  $.ajax({
    //    type: 'POST',
    //    url: "<?php echo base_url('beranda/create4')?>",
    //    data: data,
    //    async: false,
    //    dataType: 'json',
    //    success: function(response) {
    //      //console.log(data);
    //    }
    //  });
    // });

    // $(".tombol-simpan4a").click(function(){
    //  var data = $('#form-user4').serialize();
    //  $.ajax({
    //    type: 'POST',
    //    url: "<?php echo base_url('beranda/create4a')?>",
    //    data: data,
    //    async: false,
    //    dataType: 'json',
    //    success: function(response) {
    //      //console.log(data);
    //    }
    //  });
    // });



  });
</script>