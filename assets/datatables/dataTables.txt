{
    "sEmptyTable":     "Tidak ada data yang tersedia dalam system",

    "sInfo":           "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
    "sInfoEmpty":      "Menampilkan 0 sampai 0 dari 0 entri",
    "sInfoFiltered":   "(disaring dari _MAX_)",
    "sInfoPostFix":    "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Loading...",
    "sSearch":         "Cari:",
    "sZeroRecords":    "Tidak ditemukan data yang sesuai",
    "oPaginate": {
        "sFirst":    "<<",
        "sLast":     ">>",
        "sNext":     ">",
        "sPrevious": "<"
    },
    "oAria": {
        "sSortAscending":  ": activate to sort column ascending",
        "sSortDescending": ": activate to sort column descending"
    }
}